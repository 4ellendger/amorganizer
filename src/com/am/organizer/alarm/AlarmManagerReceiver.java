package com.am.organizer.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.widget.Toast;

import com.am.organizer.data.Alarm;
import com.am.organizer.data.DBHelpers;

public class AlarmManagerReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, AlarmManagerReceiver.class.getSimpleName());
        wl.acquire();

        String toastMessage;
        try {
            long id = Long.parseLong(intent.getType());
            Alarm alarm = DBHelpers.getAlarm(id);
            if (alarm != null) {
                toastMessage = "get alarm " + alarm.getName();
            } else {
                toastMessage = "alarm with id " + id + " is not exists!";
            }
            // TODO add real handler - convert it to Activity.
        } catch (NumberFormatException e) {
            toastMessage = "intent has not alarm id!";
        }
        Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show();
        wl.release();
    }
}
