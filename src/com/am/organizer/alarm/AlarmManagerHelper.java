package com.am.organizer.alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.data.Alarm;

public class AlarmManagerHelper {

    /**
     * Sets alarm for specified {@link Alarm} object.
     * 
     * @param context {@link Context} to set alarm.
     * @param alarm {@link Alarm} object with properties.
     */
    public static void setOneEventAlarm(Context context, Alarm alarm) {
        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmManagerReceiver.class);
        intent.setType(String.valueOf(alarm.getId()));// Alarms should have unique type, unique extra not enough - one "cancel" will remove all alarms.
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
        am.set(AlarmManager.RTC_WAKEUP, alarm.getTime(), alarmIntent);

        Log.d(App.TAG, "set alarm with id " + alarm.getId() + " on " + alarm.getFormattedTimeAndDate());
    }

    /**
     * Canceled alarm for specified ID.
     * 
     * @param context {@link Context} to cancel alarm.
     * @param alarmId ID of alarm to cancel.
     */
    public static void cancelAlarmById(Context context, long alarmId) {
        Intent intent = new Intent(context, AlarmManagerReceiver.class);
        intent.setType(String.valueOf(alarmId));// Alarms should have unique type, unique extra not enough - one "cancel" will remove all alarms.
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);

        Log.d(App.TAG, "cancel alarm with id " + alarmId);
    }

    /**
     * Synchronize specified {@link Alarm} with system {@link AlarmManager}. Shows toast with result.
     * 
     * @param context Used {@link Context}.
     * @param alarm {@link Alarm} to synchronize.
     */
    public static void syncAlarm(Context context, Alarm alarm) {

        // In any case cancel previous task of specified alarm.
        cancelAlarmById(context, alarm.getId());

        // If alarm enabled then setup it.
        if (alarm.isEnabled()) {
            String toastMessage;
            if (alarm.getTime() >= System.currentTimeMillis()) {
                setOneEventAlarm(context, alarm);
                toastMessage = context.getString(R.string.alarm_saved_toast, alarm.getFormattedDate(),
                        alarm.getName());
            } else {
                toastMessage = context.getString(R.string.alarm_is_late, alarm.getName(), alarm.getFormattedDate());
            }
            Toast.makeText(context, toastMessage, Toast.LENGTH_SHORT).show();
        }
    }
}
