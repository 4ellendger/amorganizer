package com.am.organizer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.am.organizer.data.Week;
import com.am.organizer.ui.AlarmEditActivity;
import com.am.organizer.ui.AlarmsActivity;
import com.am.organizer.ui.NoteEditActivity;
import com.am.organizer.ui.NotesActivity;
import com.am.organizer.ui.helpers.CalendarListAdapter;
import com.am.organizer.ui.helpers.UIHelpers;
import com.am.organizer.ui.view.DayView;

/**
 * Launch activity. Calendar.
 */
public class CalendarActivity extends Activity implements OnClickListener, OnLongClickListener {
    private static int sCurrentWeekPositionOnScreen = 2;
    private ListView mCalendarList;
    private int[] mDayNamesViewsIds = { R.id.day1, R.id.day2, R.id.day3, R.id.day4, R.id.day5, R.id.day6, R.id.day7 };
    private CalendarListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        App.init(getApplicationContext());
        setContentView(R.layout.activity_calendar);

        // Full up day names header.
        for (int i = 0; i < mDayNamesViewsIds.length; i++) {
            TextView dayName = (TextView) findViewById(mDayNamesViewsIds[i]);
            dayName.setText(Week.getDayNames()[i]);
        }

        // Setup weeks list view.
        mCalendarList = (ListView) findViewById(R.id.list);
        CalendarListAdapter.setParent(this);
        mAdapter = new CalendarListAdapter(getApplicationContext());
        mCalendarList.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.calendar_menu, menu);  
        UIHelpers.scaleMenuItems(this, menu, new int[] {R.id.action_alarms, R.id.action_notes, R.id.action_to_current});
        return true;
    }

    protected void onResume () {
        super.onResume();
        App.updateCurrentDate();// Update to current time.
        refresh();
    }

    public void refresh() {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                mAdapter = new CalendarListAdapter(getApplicationContext());
                mCalendarList.setAdapter(mAdapter);
                scrollToCurrent();
            }
        });
    }

    private void scrollToCurrent() {
        int scrollTop = App.getCurrentWeekIndex() - sCurrentWeekPositionOnScreen;
        if (scrollTop < 0) {
            scrollTop = 0;
        }
        mCalendarList.setSelection(scrollTop);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.action_to_current:
            scrollToCurrent();
            break;
        case R.id.action_alarms:
            startActivity(new Intent(App.getContext(), AlarmsActivity.class));
            break;
        case R.id.action_notes:
            startActivity(new Intent(App.getContext(), NotesActivity.class));
            break;
        case R.id.help:
            UIHelpers.showHelp(this);
            break;
        case R.id.settings:
            UIHelpers.showSettings(this);
            break;
        default:
            Toast.makeText(this, "wtf? id=" + item.getItemId(), Toast.LENGTH_SHORT).show();
            break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        DayView dayView = (DayView) view;
        dayView.setTemporaryBackgroundColor(Color.BLUE);
        Intent intent = new Intent(getApplicationContext(), AlarmEditActivity.class);
        intent.putExtra(App.EXTRA_TIME, dayView.getTime());
        startActivity(intent);
    }

    @Override
    public boolean onLongClick(View view) {
        DayView dayView = (DayView) view;
        dayView.setTemporaryBackgroundColor(Color.RED);
        Intent intent = new Intent(getApplicationContext(), NoteEditActivity.class);
        intent.putExtra(App.EXTRA_TIME, dayView.getTime());
        startActivity(intent);
        return true;
    }
}
