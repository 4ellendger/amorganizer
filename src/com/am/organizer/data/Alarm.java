package com.am.organizer.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.am.organizer.App;
import com.am.organizer.R;

/**
 * Model of Alarm table entry.
 */
public class Alarm {
    private long id = App.EMPTY_ID;
    private long noteId = App.EMPTY_ID;
    private String name = App.EMPTY_STRING;
    private boolean isEnabled = false;
    private long time = App.EMPTY_TIME;
    private boolean isDeleteOnEvent = true;
    private String musicPath = App.EMPTY_STRING;
    private boolean isVibro = true;
    private String period = App.EMPTY_STRING;
    private long bother = App.EMPTY_TIME;

    public Alarm() { }

    public Alarm(long id, long noteId, String name, boolean isEnabled, long time, boolean isDeleteOnEvent,
            String musicPath, boolean isVibro, String period, long bother) {
        this.id = id;
        this.noteId = noteId;
        this.name = name;
        this.isEnabled = isEnabled;
        this.time = time;
        this.isDeleteOnEvent = isDeleteOnEvent;
        this.period = period;
        this.musicPath = musicPath;
        this.isVibro = isVibro;
        this.bother = bother;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public long getTime() {
        return time;
    }

    public long getNoteId() {
        return noteId;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public boolean isDeleteOnHappen() {
        return isDeleteOnEvent;
    }

    public String getPeriod() {
        return period;
    }

    public String getMusicPath() {
        return musicPath;
    }

    public boolean isVibro() {
        return isVibro;
    }

    public long getBother() {
        return bother;
    }

    public String getFormattedDate() {
        return getFormattedDate(time);
    }

    public static String getFormattedDate(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd", App.getCurrentLocale());
        return formatter.format(time);
    }

    public String getFormattedTime() {
        return getFormattedTime(time);
    }

    public static String getFormattedTime(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm", App.getCurrentLocale());
        return formatter.format(time);
    }

    public static String getFormattedTimeAndDate(long time) {
        return getFormattedDate(time) + " " + getFormattedTime(time);
    }

    public String getFormattedTimeAndDate() {
        return getFormattedTimeAndDate(time);
    }

    public String getFormattedBother() {
        return getFormattedDelay(bother);
    }

    /**
     * Returns human-welcome string with delay. In resolution days..minutes.
     * 
     * @param delay Time in ms to format.
     * @return String with delay description.
     */
    public static String getFormattedDelay(long delay) {
        String delayString = App.EMPTY_STRING;
        Calendar c = Calendar.getInstance(App.getCurrentLocale());
        c.setTimeInMillis(delay - c.getTimeZone().getOffset(delay));
        if (c.get(Calendar.DAY_OF_YEAR) != 1) {
            delayString += c.get(Calendar.DAY_OF_YEAR) + " " + App.getResources().getString(R.string.days) + " ";
        }
        if (c.get(Calendar.HOUR_OF_DAY) != 0) {
            delayString += c.get(Calendar.HOUR_OF_DAY) + " " + App.getResources().getString(R.string.hours) + " ";
        }
        if (c.get(Calendar.MINUTE) != 0 || delayString == App.EMPTY_STRING) {
            delayString += c.get(Calendar.MINUTE) + " " + App.getResources().getString(R.string.mins);
        }
        return delayString;
    }

    public void setNoteId(long noteId) {
        this.noteId = noteId;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public void setDeleteOnEvent(boolean isDeleteOnEvent) {
        this.isDeleteOnEvent = isDeleteOnEvent;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public void setMusicPath(String musicPath) {
        this.musicPath = musicPath;
    }

    public void setVibro(boolean isVibro) {
        this.isVibro = isVibro;
    }

    public void setBother(long bother) {
        this.bother = bother;
    }
}
