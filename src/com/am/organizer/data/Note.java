package com.am.organizer.data;

import java.text.SimpleDateFormat;

import com.am.organizer.App;

/**
 * Model of Note table entry.
 */
public class Note {
    public static final int NAME_FROM_TEXT_LENGTH = 20; 

    private long id = App.EMPTY_ID;
    private long alarmId = App.EMPTY_ID;
    private String name = App.EMPTY_STRING;
    private String text = App.EMPTY_STRING;
    private String filePath = App.EMPTY_STRING;
    private long updateTime = App.EMPTY_TIME;

    public Note(long id, long alarmId, String name, String text, String filePath, long updateTime) {
        this.id = id;
        this.alarmId = alarmId;
        this.name = name;
        this.text = text;
        this.filePath = filePath;
        this.updateTime = updateTime;
    }

    public Note(long id, long alarmId, String name, String filePath, long updateTime) {
        this.id = id;
        this.alarmId = alarmId;
        this.name = name;
        this.filePath = filePath;
        this.updateTime = updateTime;
    }

    public Note(String text, String name, String filePath, long updateTime) {
        this.text = text;
        this.name = name;
        this.filePath = filePath;
        this.updateTime = updateTime;
    }

    public Note(long alarmId, String text, String name) {
        this.alarmId = alarmId;
        this.text = text;
        this.name = name;
        this.updateTime = System.currentTimeMillis();
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public long getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(long alarmId) {
        this.alarmId = alarmId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        updateTime();
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        updateTime();
        this.text = text;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String path) {
        this.filePath = path;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    private void updateTime() {
        this.updateTime = System.currentTimeMillis();
    }

    public static String getFormattedTime(long updateTime) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", App.getCurrentLocale());
        return formatter.format(updateTime);
    }

    public String getFormattedTime() {
        return getFormattedTime(updateTime);
    }
}
