package com.am.organizer.data;

import java.util.List;

import android.content.Context;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.alarm.AlarmManagerHelper;
import com.am.organizer.data.helpers.FileSystemHelper;

/**
 * Data manager.
 */
public class DataManager {

    /**
     * Saved specified {@link Note} in DB and in file if need. Shows toast.
     * 
     * @param context Used {@link Context}.
     * @param note {@link Note} to save.
     * @return <code>true</code> if note saved, <code>false</code> otherwise.
     */
    public static boolean saveNote(Context context, Note note) {
        if (note == null) return false;
        boolean result = false;
        long id = note.getId();
        if (id == App.EMPTY_ID) {
            id = DBHelpers.addNote(note);
        } else {
            result = DBHelpers.updateNote(note);
        }
        if (result == false || id == App.EMPTY_ID) {
            Toast.makeText(context, context.getString(R.string.note_not_saved_toast, note.getName()), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            if (App.isSyncWithFolder()) {
                result = FileSystemHelper.saveNote(context, note);
            }
            if (result) {
                Toast.makeText(context, context.getString(R.string.note_saved_toast, note.getName()), Toast.LENGTH_SHORT).show();
                return true;
            } else {
                Toast.makeText(context, context.getString(R.string.note_not_saved_in_file_toast, note.getName(), note.getFilePath()), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }

    /**
     * Saved specified {@link Alarm} in DB and in file if need. Shows toast.
     * 
     * @param context Used {@link Context}.
     * @param alarm {@link Alarm} to save.
     * @return <code>true</code> if alarm saved, <code>false</code> otherwise.
     */
    public static boolean saveAlarm(Context context, Alarm alarm) {
        if (alarm == null) return false;
        boolean result = false;
        long id = alarm.getId();
        if (id == App.EMPTY_ID) {
            id = DBHelpers.addAlarm(alarm);
        } else {
            result = DBHelpers.updateAlarm(alarm);
        }
        if (result == false || id == App.EMPTY_ID) {
            Toast.makeText(context, context.getString(R.string.alarm_not_saved_toast, alarm.getFormattedDate(), alarm.getName()), Toast.LENGTH_SHORT).show();
            return false;
        } else {
            AlarmManagerHelper.syncAlarm(context, alarm);
            Toast.makeText(context, context.getString(R.string.alarm_saved_toast, alarm.getFormattedDate(), alarm.getName()), Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    /**
     * Removed specified {@link Alarm} from DB. Shows toast if success.
     * 
     * @param context Used {@link Context}.
     * @param note {@link Note} to remove.
     * @return <code>true</code> if removed, <code>false</code> otherwise.
     */
    public static boolean removeNote(Context context, Note note) {
        if (note == null || note.getId() == App.EMPTY_ID) return false;
        if (DBHelpers.removeNote(note.getId())) {
            Toast.makeText(context, context.getString(R.string.note_removed_toast, note.getName()), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    /**
     * Removed specified {@link Alarm} from DB. Shows toast if success.
     * 
     * @param context Used {@link Context}.
     * @param alarm {@link Alarm} to remove.
     * @return <code>true</code> if removed, <code>false</code> otherwise.
     */
    public static boolean removeAlarm(Context context, Alarm alarm) {
        if (alarm == null || alarm.getId() == App.EMPTY_ID) return false;
        if (DBHelpers.removeAlarm(alarm.getId())) {
            Toast.makeText(context, context.getString(R.string.alarm_removed_toast, alarm.getName()), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    /**
     * Disabled all alarms.
     * 
     * @param context Used {@link Context}.
     */
    public static void disableAllAlarms(Context context) {
        for (Alarm alarm : DBHelpers.getAlarms(null)) {
            AlarmManagerHelper.cancelAlarmById(context, alarm.getId());
        }
    }

    /**
     * Remove (with disabling) all alarms.
     * 
     * @param context Used {@link Context}.
     */
    public static void removeAllAlarms(Context context) {
        List<Alarm> alarms = DBHelpers.getAlarms(null);
        for (Alarm alarm : alarms) {
            AlarmManagerHelper.cancelAlarmById(context, alarm.getId());
        }
        DBHelpers.removeAllAlarms();
    }
}
