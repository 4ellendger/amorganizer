package com.am.organizer.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.am.organizer.App;
import com.am.organizer.data.helpers.DBHelper;
import com.am.organizer.ui.AlarmsActivity.AlarmsOrder;
import com.am.organizer.ui.NotesActivity;
import com.am.organizer.ui.NotesActivity.NotesOrder;

/**
 * Data base helpers.
 */
public final class DBHelpers {

    public static final String TABLE_NOTES = "Notes";
    public static final String TABLE_ALARMS = "Alarms";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ALARM_ID = "aid";
    public static final String COLUMN_NOTE_ID = "nid";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_ENABLED = "enabled";
    public static final String COLUMN_DELETE_ON_EVENT = "doe";
    public static final String COLUMN_PERIOD = "period";
    public static final String COLUMN_PATH = "path";
    public static final String COLUMN_VIBRO = "vibro";
    public static final String COLUMN_TEXT = "text";
    public static final String COLUMN_BOTHER = "bother";
    
    private static DBHelper dbHelper;
    
    static {
        dbHelper = DBHelper.getInstance();
    }

    private static ContentValues getAlarmSets(Alarm alarm) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_NOTE_ID, alarm.getNoteId());
        values.put(COLUMN_NAME, alarm.getName());
        values.put(COLUMN_ENABLED, alarm.isEnabled());
        values.put(COLUMN_TIME, alarm.getTime());
        values.put(COLUMN_DELETE_ON_EVENT, alarm.isDeleteOnHappen());
        values.put(COLUMN_PATH, alarm.getMusicPath());
        values.put(COLUMN_VIBRO, alarm.isVibro());
        values.put(COLUMN_PERIOD, alarm.getPeriod());
        values.put(COLUMN_BOTHER, alarm.getBother());
        return values;
    }

    private static ContentValues getNoteSets(Note note) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_ALARM_ID, note.getAlarmId());
        values.put(COLUMN_NAME, note.getName());
        values.put(COLUMN_TEXT, note.getText());
        values.put(COLUMN_PATH, note.getFilePath());
        values.put(COLUMN_TIME, note.getUpdateTime());
        return values;
    }

    /**
     * Returns {@link Alarm} from {@link Cursor}.
     * 
     * @param cursor {@link Cursor} which pointed to {@link Alarm} row.
     * @return {@link Alarm} object.
     */
    private static Alarm getAlarmFromCursor(Cursor cursor) {
        return new Alarm(cursor.getLong(0),
                cursor.getLong(1),
                cursor.getString(2),
                cursor.getInt(3) != 0,
                cursor.getLong(4),
                cursor.getInt(5) != 0,
                cursor.getString(6),
                cursor.getInt(7) != 0,
                cursor.getString(8),
                cursor.getLong(9));
    }

    /**
     * Returns {@link Note} from {@link Cursor}.
     * 
     * @param cursor {@link Cursor} which pointed to {@link Note} row.
     * @return {@link Note} object.
     */
    private static Note getNoteFromCursor(Cursor cursor) {
        return new Note(cursor.getLong(0),
                cursor.getLong(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getLong(5));
    }

    /**
     * Added {@link Alarm} to database.
     * 
     * @param alarm {@link Alarm} to add.
     * @return ID of created row, {@code -1} otherwise.
     */
    public synchronized static long addAlarm(Alarm alarm) {
        return dbHelper.getDb().insert(TABLE_ALARMS, null, getAlarmSets(alarm));
    }

    /**
     * Added {@link Note} to database.
     * 
     * @param note {@link Note} to add.
     * @return ID of created row, {@code -1} otherwise.
     */
    public synchronized static long addNote(Note note) {
        return dbHelper.getDb().insert(TABLE_NOTES, null, getNoteSets(note));
    }

    /**
     * Returns list of {@link Alarms}s with ordered by specified value and with all fields.
     * 
     * @param order {@link AlarmsOrder} of notes.
     * @return List of {@link Alarms}s. Not returns <code>null</code>;
     */
    public static synchronized List<Alarm> getAlarms(AlarmsOrder order) {
        List<Alarm> alarms = new ArrayList<Alarm>();
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT * FROM ").append(TABLE_ALARMS);
        boolean isNeedReorder = false;
        if (order == null) order = AlarmsOrder.BY_TIME_NEW;
        switch (order) {
        case BY_TIME_NEW:
            builder.append(" ORDER BY ");
            builder.append(COLUMN_TIME).append(" DESC");
            break;
        case BY_TIME_OLD:
            builder.append(" ORDER BY ");
            builder.append(COLUMN_TIME).append(" ASC");
            break;
        case FIRST_PERIODIC_BY_TIME_NEW:
            builder.append(" ORDER BY ");
            builder.append("CASE WHEN ").append(COLUMN_PERIOD).append(" IS NOT '', ").append(COLUMN_TIME);
            break;
        default:
            isNeedReorder = true;
            break;
        }
        SQLiteDatabase db = dbHelper.getDb();
        db.beginTransaction();
        try {
            Cursor c = db.rawQuery(builder.toString(), null);
            if (c != null && c.moveToFirst()) {
                do {
                    alarms.add(getAlarmFromCursor(c));
                } while (c.moveToNext());
                c.close();
            }
        } finally {
            db.endTransaction();
        }

        // If cannot write query for specified order to SQLite then sort manually.
        if (isNeedReorder) {
            List<Note> notes;
            switch (order) {
            case BY_NOTE_NAME_Z:
                notes = getNotes(NotesOrder.BY_NAME_Z);
                break;
            case BY_NOTE_NAME_A:
            default:
                notes = getNotes(NotesOrder.BY_NAME_A);
                break;
            }

            // Prepare array for ordered alarms and array of "isMoved" values.
            List<Alarm> orderedAlarms = new ArrayList<Alarm>();
            boolean[] movedAlarms = new boolean[alarms.size()];
            Arrays.fill(movedAlarms, false);

            // First move in orderedAlarms alarms which have notes in notes order.
            for (int i = 0; i < notes.size(); i++) {
                Note note = notes.get(i);
                long noteId = note.getId();
                for (int j = 0; j < alarms.size(); j++) {
                    Alarm currentAlarm = alarms.get(j);
                    long currentNoteId = currentAlarm.getNoteId();
                    if (currentNoteId == noteId) {
                        orderedAlarms.add(currentAlarm);
                        movedAlarms[j] = true;
                    }
                }
            }

            // Second move alarms without notes in initial order.
            for (int i = 0; i < movedAlarms.length; i++) {
                if (!movedAlarms[i]) {
                    orderedAlarms.add(alarms.get(i));
                }
            }
            alarms = orderedAlarms;
        }
        return alarms;
    }

    /**
     * Returns list of {@link Note}s with ordered by specified value and with ID, name, path, time.
     * 
     * @param order {@link NotesOrder} of notes.
     * @return List of {@link Note}s. Not returns <code>null</code>;
     */
    public synchronized static List<Note> getNotes(NotesActivity.NotesOrder order) {
        StringBuilder builder = new StringBuilder();
        builder.append("SELECT ").append(COLUMN_ID).append(",");
        builder.append(COLUMN_ALARM_ID).append(",");
        builder.append(COLUMN_NAME).append(",");
        builder.append(COLUMN_PATH).append(",");
        builder.append(COLUMN_TIME);
        builder.append(" FROM ").append(TABLE_NOTES);
        builder.append(" ORDER BY ");
        if (order == null) order = NotesOrder.BY_NAME_A;
        switch (order) {
        case BY_NAME_A:
            builder.append(COLUMN_NAME).append(" ASC");
            break;
        case BY_NAME_Z:
            builder.append(COLUMN_NAME).append(" DESC");
            break;
        case BY_TIME_NEW:
            builder.append(COLUMN_TIME).append(" DESC");
            break;
        case BY_TIME_OLD:
            builder.append(COLUMN_TIME).append(" ASC");
            break;
        default:
            break;
        }

        List<Note> notes = new ArrayList<Note>();
        SQLiteDatabase db = dbHelper.getDb();
        db.beginTransaction();
        try {
            Cursor c = dbHelper.getDb().rawQuery(builder.toString(), null);
            if (c != null && c.moveToFirst()) {
                do {
                    notes.add(new Note(c.getLong(0),
                            c.getLong(1),
                            c.getString(2),
                            c.getString(3),
                            c.getLong(4)));
                } while (c.moveToNext());
                c.close();
            }
        } finally {
            db.endTransaction();
        }
        return notes;
    }

    /**
     * Returns {@link Alarm} object.
     * 
     * @param alarmId Id of alarm.
     * @return {@link Alarm} objects if exist, <code>null</code> otherwise.
     */
    public synchronized static Alarm getAlarm(long alarmId) {
        SQLiteDatabase db = dbHelper.getDb();
        Alarm alarm = null;
        db.beginTransaction();
        try {
            Cursor c = db.rawQuery("SELECT * FROM " + TABLE_ALARMS + " WHERE " + COLUMN_ID + "=?", new String[] { String.valueOf(alarmId) });
            if (c != null && c.moveToFirst()) {
                alarm = getAlarmFromCursor(c);
                c.close();
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        return alarm;
    }

    /**
     * Returns {@link Note} object.
     * 
     * @param mNoteId Id of note.
     * @return {@link Note} objects if exist, <code>null</code> otherwise.
     */
    public synchronized static Note getNote(long mNoteId) {
        Note note = null;
        SQLiteDatabase db = dbHelper.getDb();
        db.beginTransaction();
        try {
            Cursor c = dbHelper.getDb().rawQuery("SELECT * FROM " + TABLE_NOTES + " WHERE " + COLUMN_ID + "=?", new String[] { String.valueOf(mNoteId) });
            if (c != null && c.moveToFirst()) {
                note = getNoteFromCursor(c);
                c.close();
            }
        } finally {
            db.endTransaction();
        }
        return note;
    }

    /**
     * Update all fields of specified {@link Alarm}.
     * 
     * @param alarm {@link Alarm} to update.
     * @return <code>true</code> if row successfully updated, <code>false</code> otherwise.
     */
    public synchronized static boolean updateAlarm(Alarm alarm) {
        Log.d(App.TAG, "--update = " + alarm.isEnabled());

        SQLiteDatabase db = dbHelper.getDb();

        int updateResult = 0;
        db.beginTransaction();
        try {
            Cursor c = db.rawQuery("UPDATE " + TABLE_ALARMS + " SET " + COLUMN_ENABLED + "="
                    + (alarm.isEnabled() ? "1" : "0") + " WHERE " + COLUMN_ID + "=" + alarm.getId(), null);
            if (c!=null) c.close();
            
            //updateResult = db.update(TABLE_ALARMS, getAlarmSets(alarm), COLUMN_ID + "=?", new String[] { String.valueOf(alarm.getId()) });
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        Alarm al = getAlarm(alarm.getId());
        Log.d(App.TAG, "--check = " + al.isEnabled());
        
        return updateResult == 1;
    }

    /**
     * Update all fields of specified {@link Note}.
     * 
     * @param note {@link Note} to update.
     * @return <code>true</code> if row successfully updated, <code>false</code> otherwise.
     */
    public synchronized static boolean updateNote(Note note) {
        return dbHelper.getDb().update(TABLE_NOTES, getNoteSets(note), COLUMN_ID + " = " + note.getId(), null) == 1;
    }

    /**
     * Updated text and updated time for specified by ID {@link Note}.
     * 
     * @param id Id of row to update.
     * @param text Text to set.
     * @param time Update time to set.
     * @return <code>true</code> if row successfully updated, <code>false</code> otherwise.
     */
    public synchronized static boolean updateNoteTextAndTime(long id, String text, long time) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_TEXT, text);
        values.put(COLUMN_TIME, time);
        return dbHelper.getDb().update(TABLE_NOTES, values, COLUMN_ID + " = " + id, null) == 1;
    }

    public synchronized static boolean removeAlarm(long id) {
        return dbHelper.getDb().delete(TABLE_ALARMS, COLUMN_ID + " = " + id, null) == 1;
    }

    public synchronized static void removeAllAlarms() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        dbHelper.dropAlarmsTable(db);
        dbHelper.createAlarmsTable(db);
    }

    public synchronized static void removeAllNotes() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        dbHelper.dropNotesTable(db);
        dbHelper.createNotesTable(db);
    }

    public synchronized static boolean removeNote(long id) {
        return dbHelper.getDb().delete(TABLE_NOTES, COLUMN_ID + " = " + id, null) == 1;
    }

    public synchronized static void logTable(String tableName) {
        try {
            Cursor c =  dbHelper.getDb().rawQuery("SELECT * from " + tableName  , null);
            if (c != null && c.moveToFirst()) {
                Log.d(App.TAG, tableName + ": count=" + c.getCount());
                int count = c.getColumnCount();
                String rowInString;
                do {
                    rowInString = App.EMPTY_STRING;
                    for (int i = 0; i < count; i++) {
                        rowInString += c.getString(i) + ", ";
                    }
                    Log.d(App.TAG, rowInString);
                } while (c.moveToNext());
            } else {
                Log.d(App.TAG, tableName + " is empty!");
            }
        } catch (Exception e) {
            Log.e(App.TAG, "Cannot log '" + tableName + "'");
        }
    }

    /**
     * Clears all tables.
     */
    public synchronized static void clearTables() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        dbHelper.dropAlarmsTable(db);
        dbHelper.dropNotesTable(db);
        dbHelper.onCreate(db);
        Log.i(App.TAG, DBHelpers.class.getSimpleName() + ".dropTables: All tables cleared.");
    }
}
