package com.am.organizer.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.am.organizer.App;
import com.am.organizer.ui.helpers.CalendarListAdapter;

/**
 * Class with data to show week row.
 */
public class Week {
    public static final int DAYS_COUNT = 7;
    public static final int CURRENT_WEEK_CALENDAR_INDEX = Integer.MAX_VALUE / 2;
    private static SimpleDateFormat sDateFormatter;
    private static String[] dayNames = new String[DAYS_COUNT];
    
    private final int[] mDayNumbers;
    private final long[] mDayTimes;
    private String mMonthName;

    // Note that it must be called after App.init()!
    static {
        sDateFormatter = new SimpleDateFormat("EEE", App.getCurrentLocale());
        dayNames = new String[DAYS_COUNT];
        sDateFormatter.applyPattern("EEE");
        Calendar c = Calendar.getInstance();
        c.clear();
        c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek());
        for (int i = 0; i < dayNames.length; i++) {
            dayNames[i] = sDateFormatter.format(c.getTime());
            c.add(Calendar.DAY_OF_WEEK, 1);
        }
    }

    /**
     * Constructor of {@link Week}.
     * 
     * @param startDayCalendar {@link Calendar} object which contains start day of week.
     */
    private Week(Calendar startDayCalendar) {
        mDayNumbers = new int[DAYS_COUNT];
        int month = startDayCalendar.get(Calendar.MONTH);
        int year = startDayCalendar.get(Calendar.YEAR);
        sDateFormatter.applyPattern("yyyy");
        String yearString = sDateFormatter.format(startDayCalendar.getTime());
        sDateFormatter.applyPattern("MMM");
        mMonthName = sDateFormatter.format(startDayCalendar.getTime());

        // Full up days and day times;
        sDateFormatter.applyPattern("EEE");
        mDayTimes = new long[DAYS_COUNT];
        for (int i = 0; i < DAYS_COUNT; i++) {
            mDayTimes[i] = startDayCalendar.getTimeInMillis();
            mDayNumbers[i] = startDayCalendar.get(Calendar.DAY_OF_MONTH);
            startDayCalendar.add(Calendar.DATE, 1);
        }

        // Full up monthName.
        if (startDayCalendar.get(Calendar.MONTH) == month) {
            mMonthName += " " + yearString;
        } else {
            if (startDayCalendar.get(Calendar.YEAR) != year) {
                mMonthName += " " + yearString;
            }
            sDateFormatter.applyPattern(" / MMM yyyy");
            mMonthName += sDateFormatter.format(startDayCalendar.getTime());
        }
    }

    public static String[] getDayNames() {
        return dayNames;
    }

    public int getDayNumberForDay(int dayIndex) {
        return mDayNumbers[dayIndex];
    }

    public long getTimeForDay(int dayIndex) {
        return mDayTimes[dayIndex];
    }

    public String getMonthName() {
        return mMonthName;
    }

    /**
     * Returns {@link Week} by index relative to {@link CalendarListAdapter#getStartWeekIndex()}.
     * 
     * @param index Week index to get.
     * @return {@link Week} object.
     */
    public static Week getWeekByCalendarIndex(int index) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int weekOfYear = calendar.get(Calendar.WEEK_OF_YEAR);

        // Check index of week relative to start week;
        int offset = index - App.getCurrentWeekIndex();
        weekOfYear += offset;
        calendar.clear();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.WEEK_OF_YEAR, weekOfYear);
        //Log.d(CalendarActivity.TAG, "getWeekByCalendarIndex, year=" + year + ", weekOfYear=" + weekOfYear);
        return new Week(calendar);
    }
}
