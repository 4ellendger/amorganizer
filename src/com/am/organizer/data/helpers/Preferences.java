package com.am.organizer.data.helpers;

import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.ui.AlarmsActivity.AlarmsOrder;
import com.am.organizer.ui.NotesActivity.NotesOrder;

/**
 * Contains getters/setters for android preferences.
 */
public class Preferences {
    public static final boolean DEFAULT_SYNC_WITH_FOLDER = false;
    public static final String DEFAULT_SYNC_FOLDER_PATH = App.getContext().getString(R.string.sync_folder_default);
    public static final NotesOrder DEFAULT_NOTES_ORDER = NotesOrder.BY_NAME_A;
    public static final boolean DEFAULT_SET_DATE_IN_NEW_NOTE = true;
    public static final AlarmsOrder DEFAULT_ALARMS_ORDER = AlarmsOrder.BY_TIME_NEW;
    public static final long DEFAULT_TIME_DELAY = TimeUnit.HOURS.toMillis(8);
    public static final boolean DEFAULT_ALARM_DELETE_ON_HAPPEN = true;
    public static final String DEFAULT_MUSIC_PATH = App.EMPTY_STRING;
    public static final boolean DEFAULT_VIBRATION = true;
    public static final String DEFAULT_PERIOD = App.EMPTY_STRING;
    public static final long DEFAULT_BOTHER = TimeUnit.MINUTES.toMillis(5);

    /**
     * Updates data in {@link App} singleton.
     * 
     * @param context Used {@link Context}.
     */
    public static void updateConfiguration(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        App.updateFromPreferences(
                prefs.getBoolean(
                        context.getString(R.string.pref_sync_with_sdcard),
                        DEFAULT_SYNC_WITH_FOLDER),
                        getSyncFolderPath(context),
                        getNotesOrder(context),
                        prefs.getBoolean(context.getString(R.string.pref_date_in_new_note),
                        DEFAULT_SET_DATE_IN_NEW_NOTE),
                        getAlarmsOrder(context),
                        prefs.getLong(context.getString(R.string.pref_time_delay),
                        DEFAULT_TIME_DELAY),
                        prefs.getBoolean(context.getString(R.string.pref_del_on_happen),
                        DEFAULT_ALARM_DELETE_ON_HAPPEN),
                        prefs.getString(context.getString(R.string.pref_music),
                        DEFAULT_MUSIC_PATH),
                        prefs.getBoolean(context.getString(R.string.pref_vibro),
                        DEFAULT_VIBRATION),
                        prefs.getString(context.getString(R.string.pref_period),
                        DEFAULT_PERIOD),
                        prefs.getLong(context.getString(R.string.pref_bother),
                        DEFAULT_BOTHER));
    }

    /**
     * Returns current notes order.
     * 
     * @param context Used {@link Context}.
     * @return Notes order.
     */
    public static int getNotesOrder(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return Integer.parseInt(prefs.getString(context.getString(R.string.pref_choose_notes_order),
                        String.valueOf(DEFAULT_NOTES_ORDER.ordinal())));
    }

    /**
     * Returns current alarms order.
     * 
     * @param context Used {@link Context}.
     * @return Alarms order.
     */
    public static int getAlarmsOrder(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return Integer.parseInt(prefs.getString(context.getString(R.string.pref_choose_alarms_order),
                        String.valueOf(DEFAULT_ALARMS_ORDER.ordinal())));
    }

    /**
     * Returns current path to sync folder.
     * 
     * @param context Used {@link Context}.
     * @return Path to sync folder.
     */
    public static String getSyncFolderPath(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(
                context.getString(R.string.pref_sync_folder), DEFAULT_SYNC_FOLDER_PATH);
    }

    /**
     * Sets sync folder path.
     * 
     * @param context Used {@link Context}.
     * @param path Path to set.
     */
    public static void setSyncFolderPath(Context context, String path) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putString(context.getString(R.string.pref_sync_folder), path);
        editor.commit();
    }

    /**
     * Returns current delay for new alarms.
     * 
     * @param context Used {@link Context}.
     * @return Delay for new alarms in ms.
     */
    public static long getAlarmTimeDelay(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(
                context.getString(R.string.pref_time_delay), DEFAULT_TIME_DELAY);
    }

    /**
     * Sets delay for new alarms.
     * 
     * @param context Used {@link Context}.
     * @param delay Delay to set.
     */
    public static void setAlarmTimeDelay(Context context, long delay) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putLong(context.getString(R.string.pref_time_delay), delay);
        editor.commit();
    }

    /**
     * Returns current period pattern.
     * 
     * @param context Used {@link Context}.
     * @return Period string pattern.
     */
    public static String getPeriod(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(
                context.getString(R.string.pref_period), DEFAULT_PERIOD);
    }

    /**
     * Returns current bother delay.
     * 
     * @param context Used {@link Context}.
     * @return Bother delay in ms.
     */
    public static long getBother(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getLong(
                context.getString(R.string.pref_bother), DEFAULT_BOTHER);
    }

    /**
     * Sets bother delay.
     * 
     * @param context Used {@link Context}.
     * @param botherDelay Bother delay.
     */
    public static void setBother(Context context, long botherDelay) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        editor.putLong(context.getString(R.string.pref_bother), botherDelay);
        editor.commit();
    }

    /**
     * Returns current alarm music path.
     * 
     * @param context Used {@link Context}.
     * @return Alarm music path.
     */
    public static String getAlarmMusicPath(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(
                context.getString(R.string.pref_music), DEFAULT_MUSIC_PATH);
    }
}
