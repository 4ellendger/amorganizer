package com.am.organizer.data.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.data.DBHelpers;
import com.am.organizer.data.Note;
import com.am.organizer.ui.helpers.DialogsHelper;
import com.am.organizer.ui.helpers.RefreshableActivity;

/**
 * Helpers to work with file system.
 */
public class FileSystemHelper {
    public static final int READ_FILE_BUFFER_SIZE = 1024;
    public static final String TXT_EXTENSION = ".txt";

    /**
     * Created new {@link Note} from specified file.
     * 
     * @param file File to created {@link Note}.
     * @return {@link Note} from specified file.
     */
    public static Note getNoteFromFile(File file) {
        BufferedReader fileReader = null;
        try {
            fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath())));
            /*} catch (UnsupportedEncodingException nsee) {
            Toast.makeText(App.getContext(), App.getResources().getString(R.string.encoding_not_utf8, file.getAbsolutePath()), Toast.LENGTH_SHORT).show();
            Log.e(App.TAG, null, nsee);*/
        } catch (FileNotFoundException fnfe) {
            Log.e(App.TAG, FileSystemHelper.class.getSimpleName()
                    + ".getNoteFromFile: Wrong file name '" + file.getAbsolutePath() + "'.", fnfe);
        }
        char[] buffer = new char[READ_FILE_BUFFER_SIZE];
        StringBuilder builder = new StringBuilder(App.EMPTY_STRING);
        int count = 0;
        try {
            while ((count = fileReader.read(buffer)) != -1) {
                builder.append(buffer, 0, count);
            }
        } catch (IOException e) {
            Log.e(App.TAG, FileSystemHelper.class.getSimpleName()
                    + ".getNoteFromFile: Cannot read file '" + file.getAbsolutePath() + "'.", e);
        } finally {
            try {
                fileReader.close();
            } catch (IOException e) {
                Log.e(App.TAG, FileSystemHelper.class.getSimpleName()
                        + ".getNoteFromFile: Cannot close BufferedReader for file '" + file.getAbsolutePath() + "'.", e);
            }
        }
        String name = file.getName();
        name = name.substring(0, name.length() - 4);// Remove TXT_EXTENSION.
        return new Note(builder.toString(), name, file.getAbsolutePath(), file.lastModified());
    }

    /**
     * Created notes from all TXT files in specified folder.
     * 
     * @param pathToFolder Path to folder to create notes.
     * @param isRecursively <code>true</code> if for all subfolders too.
     * @return List of created notes.
     */
    public static List<Note> createNotesFromFolder(String pathToFolder, boolean isRecursively) {
        File folder = new File(pathToFolder);
        List<Note> notes = new ArrayList<Note>();
        if (folder.exists() && folder.isDirectory()) {
            File[] files = folder.listFiles();
            for (File file : files) {
                if (file.isFile() && file.getName().toLowerCase(App.getCurrentLocale()).contains(TXT_EXTENSION)) {
                    if (file.canRead() && file.length() < App.MAX_NOTE_BYTES_LENGHT) {
                        notes.add(getNoteFromFile(file));
                    }
                } else if (file.isDirectory() && isRecursively) {
                    notes.addAll(createNotesFromFolder(file.getAbsolutePath(), true));
                }
            }
        }
        return notes;
    }

    /**
     * Imports all TXT files from specified folder as {@link Note}s. Works in new thread. Shows toast if success.
     * 
     * @param activity {@link Activity} from which called.
     * @param folder path to folder to import.
     */
    public static void importNotes(final RefreshableActivity activity, final String folder) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                List<Note> importedNotes = FileSystemHelper.createNotesFromFolder(folder, false);
                List<Note> notesToAdd = new ArrayList<Note>();
                List<Note> notesToUpdate = new ArrayList<Note>();

                // Iterate through all notes from files and full up notesToAdd and notesToUpdate.
                for (Note note : importedNotes) {
                    boolean isNeedAdd = true;

                    for (final Note existsNote : DBHelpers.getNotes(null)) {
                        if (existsNote.getName().equals(note.getName())) {
                            int choice = DialogsHelper.askUserButton(activity,
                                    activity.getString(R.string.replace_note_confirmation, existsNote.getName(), existsNote.getFilePath(), note.getFilePath()),
                                    new String[] { activity.getString(R.string.abort_import), activity.getString(R.string.yes), activity.getString(R.string.no)});
                            switch (choice) {
                            case DialogInterface.BUTTON_NEGATIVE:

                                // Abort.
                                activity.runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        Toast.makeText(activity,
                                                App.getResources().getString(R.string.import_aborted, folder),
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                                return;
                            case DialogInterface.BUTTON_POSITIVE:

                                // Add to notesToUpdate.
                                note.setId(existsNote.getId());
                                notesToUpdate.add(note);
                                isNeedAdd = false;
                                break;
                            case DialogInterface.BUTTON_NEUTRAL:

                                // Skip.
                                isNeedAdd = false;
                                break;
                            default:
                                Toast.makeText(activity, "wtf? choice=" + choice, Toast.LENGTH_SHORT).show();
                                break;
                            }
                        }
                    }

                    if (isNeedAdd) {
                        notesToAdd.add(note);
                    }
                }

                // Add notes.
                for (Note note : notesToAdd) {
                    DBHelpers.addNote(note);
                }

                // Update notes.
                for (Note note : notesToUpdate) {
                    DBHelpers.updateNoteTextAndTime(note.getId(), note.getText(), note.getUpdateTime());
                }

                final int count = notesToAdd.size() + notesToUpdate.size();
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity, activity.getString(R.string.import_success, count, folder), Toast.LENGTH_SHORT).show();
                        activity.refresh();
                    }
                });
            }
        }).start();
    }

    /**
     * Exports all {@link Note}s to specified folder as TXT files. Works in new thread. Shows toast if success.
     * 
     * @param activity {@link Activity} from which called.
     * @param folder path to folder to export.
     */
    public static void exportAllNotes(final Activity activity, final String folder) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                List<Note> notes = DBHelpers.getNotes(null);
                int count = 0;
                for (Note note : notes) {
                    note.setFilePath(folder + File.separator + note.getName());
                    if (saveNote(activity, note)) {
                        count++;
                    }
                }
                Toast.makeText(activity, activity.getString(R.string.export_success, count, folder), Toast.LENGTH_SHORT).show();
            }
        }).start();
    }

    /**
     * Created or updated TXT file with note.
     * 
     * @param note {@link Note} to save/update. Should have "filePath" value.
     * @return <code>true</code> if note successfully saved, <code>false</code> otherwise.
     */
    public static boolean saveNote(Context context, Note note) {
        if (!note.getFilePath().equals(App.EMPTY_STRING)) {
            try {
                File noteFile = new File(note.getFilePath());

                // Try to create and rewrite.
                if (!noteFile.exists() || !noteFile.isFile()) {
                    noteFile.mkdirs();
                    if (!noteFile.createNewFile()) {
                        return false;// Cannot create such file.
                    }
                }

                // Write content.
                OutputStream os = new FileOutputStream(noteFile);
                os.write(note.getText().getBytes());
                os.close();
                return true;
            } catch (Exception e) {
                Log.e(App.TAG, FileSystemHelper.class.getSimpleName() + ".saveNote: catch exception: " + e.getMessage());
            }
        }
        return false;
    }
}