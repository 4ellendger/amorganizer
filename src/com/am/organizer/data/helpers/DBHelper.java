package com.am.organizer.data.helpers;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.am.organizer.App;
import com.am.organizer.data.DBHelpers;

/**
 * SQLite database helper. Please not use it directly.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "amorganizer.db";

    private static DBHelper instance;

    /** Database. */
    //private SQLiteDatabase db;

    private DBHelper() {
        super(App.getContext(), DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DBHelper getInstance() {
        if (instance == null) {
            instance = new DBHelper();
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createAlarmsTable(db);
        createNotesTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropAlarmsTable(db);
        dropNotesTable(db);
        onCreate(db);
    }

    public SQLiteDatabase getDb() {
        /*if (db == null) {
            //db = App.getContext().openOrCreateDatabase(DATABASE_NAME, Context.MODE_PRIVATE, null);
            db = getWritableDatabase();
        }*/
        return getWritableDatabase();
    }

    public void createAlarmsTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBHelpers.TABLE_ALARMS + " (" 
                + DBHelpers.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBHelpers.COLUMN_NOTE_ID + " INTEGER, "
                + DBHelpers.COLUMN_NAME + " TEXT NOT NULL, "
                + DBHelpers.COLUMN_TIME + " INTEGER, "
                + DBHelpers.COLUMN_ENABLED + " INTEGER, "
                + DBHelpers.COLUMN_DELETE_ON_EVENT + " INTEGER, "
                + DBHelpers.COLUMN_PATH + " TEXT, "
                + DBHelpers.COLUMN_VIBRO + " INTEGER, "
                + DBHelpers.COLUMN_PERIOD + " TEXT, "
                + DBHelpers.COLUMN_BOTHER + " INTEGER);");
    }

    public void createNotesTable(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DBHelpers.TABLE_NOTES + " ("
                + DBHelpers.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DBHelpers.COLUMN_ALARM_ID + " INTEGER, "
                + DBHelpers.COLUMN_NAME + " TEXT NOT NULL, "
                + DBHelpers.COLUMN_TEXT + " TEXT, "
                + DBHelpers.COLUMN_PATH + " TEXT, "
                + DBHelpers.COLUMN_TIME + " INTEGER);");
    }

    public void dropAlarmsTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + DBHelpers.TABLE_ALARMS);
    }

    public void dropNotesTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + DBHelpers.TABLE_NOTES);
    }
}
