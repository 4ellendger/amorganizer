package com.am.organizer.data.helpers;

import android.media.MediaPlayer;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.R;

public final class MusicHelper {
    private static MediaPlayer sMediaPlayer;

    public static boolean startPlay(String musicPath) {
        boolean result = false;
        if (!TextUtils.isEmpty(musicPath)) {
            if (sMediaPlayer != null) {
                sMediaPlayer = new  MediaPlayer();
            }
            try {
                sMediaPlayer.setDataSource(musicPath);
                sMediaPlayer.prepare();
                sMediaPlayer.start();
                result = true;
            } catch (Exception e) {
                String error = App.getContext().getString(R.string.cannot_play_file, musicPath);
                Log.e(App.TAG, error);
                Toast.makeText(App.getContext(), error, Toast.LENGTH_SHORT).show();
            }
        }
        return result;
    }

    public static void stopPlay() {
        if (sMediaPlayer != null) {
            sMediaPlayer.stop();
        }
    }
}
