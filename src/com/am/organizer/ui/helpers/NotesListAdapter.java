package com.am.organizer.ui.helpers;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.data.DBHelpers;
import com.am.organizer.data.Note;
import com.am.organizer.ui.NotesActivity;
import com.am.organizer.ui.NotesActivity.NotesOrder;

/**
 * List adapter for Notes activity.
 */
public class NotesListAdapter extends ArrayAdapter<Note> {
    private List<Note> mNotes;

    public NotesListAdapter(Context context, int resource, NotesOrder order) {
        super(context, resource);
        mNotes = DBHelpers.getNotes(order);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) App.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_note, null, true);
            holder = new ViewHolder();
            holder.name = (TextView) rowView.findViewById(R.id.note_name);
            holder.description = (TextView) rowView.findViewById(R.id.note_description);
            holder.fade = rowView.findViewById(R.id.note_alarm_fade);
            holder.alarm = (Button) rowView.findViewById(R.id.note_alarm);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        Note note = mNotes.get(position);
        holder.name.setText(note.getName());
        String descriptionString = "Updated: " + note.getFormattedTime();
        if (note.getFilePath() != App.EMPTY_STRING) {
            descriptionString += ", Path:" + note.getFilePath();
        }
        holder.description.setText(descriptionString);
        final Note finalNote = getItem(position);
        if (finalNote.getAlarmId() != App.EMPTY_ID) {
            holder.alarm.setOnClickListener(NotesActivity.getInstance());
            holder.alarm.setTag(position);
            holder.alarm.setEnabled(true);
        } else {
            holder.fade.setVisibility(View.VISIBLE);
            holder.alarm.setEnabled(false);
        }
        return rowView;
    }

    @Override
    public int getCount() {
        return mNotes.size();
    }

    @Override
    public Note getItem (int pos){
        return mNotes.get(pos);
    }

    /**
     * Class to save same views on rows.
     */
    static class ViewHolder {
        public TextView name;
        public TextView description;
        public Button alarm;
        public View fade;
    }

    public List<Note> getNotes() {
        return mNotes;
    }
}
