package com.am.organizer.ui.helpers;

/**
 * Interface to receive callbacks from {@link PathChooser}.
 */
public interface IPathChooseListener {

    /**
     * Called when {@link PathChooser} complete work.
     * 
     * @param isChoosed <code>true</code> if path right,
     *          <code>false</code> if pressed "Cancel" button or path is wrong.
     * @param path Path to file/folder.
     */
    public void onPathChoose(boolean isChoosed, String path);
}
