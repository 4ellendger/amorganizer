package com.am.organizer.ui.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.ui.SettingsActivity;

/**
 * Static UI helpers.
 */
public final class UIHelpers {

    /**
     * Checks that height of specified {@link MenuItem} icon not more than desired size.
     * If more then changed to icon width and height to desired size.
     * 
     * @param context Used {@link Context}.
     * @param item {@link MenuItem} to scale icon size.
     * @param desiredHeight Desired size in pixels.
     */
    public static void scaleMenuItemIcon(Context context, MenuItem item, int desiredHeight) {
        if (desiredHeight > 0) {
            Drawable original = item.getIcon();
            int hi = original.getIntrinsicHeight();
            if (hi > desiredHeight) {
                Bitmap bitmap = ((BitmapDrawable) original).getBitmap();
                Drawable scaled = new BitmapDrawable(context.getResources(), Bitmap.createScaledBitmap(
                        bitmap, desiredHeight, desiredHeight, true));
                item.setIcon(scaled);
            }
        }
    }

    /**
     * Scale specified menu items to desired height.
     * 
     * @param activity Used {@link Activity}.
     * @param menu {@link Menu} with items.
     * @param desiredHeight Desired size in pixels.
     * @param itemIds Array of item IDs.
     */
    public static void scaleMenuItems(Activity activity, Menu menu, int[] itemIds) {
        int desiredHeight = activity.getActionBar().getHeight();
        if (desiredHeight <= 0) {
            TypedValue tv = new TypedValue();
            if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                desiredHeight = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());
            }
        }
        MenuItem item;
        for (int id : itemIds) {
            item = menu.findItem(id);
            scaleMenuItemIcon(activity, item, desiredHeight);
        }
    }

    public static void showSettings(Activity callerInstance) {
        Intent settingsActivity = new Intent(callerInstance,
                SettingsActivity.class);
        callerInstance.startActivity(settingsActivity);
    }

    public static void showHelp(Activity callerInstance) {
        Toast.makeText(App.getContext(), "showHelp, callerInstance=" + callerInstance, Toast.LENGTH_SHORT).show();
    }
}
