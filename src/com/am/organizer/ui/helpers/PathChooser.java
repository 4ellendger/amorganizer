package com.am.organizer.ui.helpers;

import java.io.File;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.widget.EditText;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.R;

/**
 * UI helpers to choose file or folder.<br/>
 * Used from Activities with 3 steps:
 * <ol>
 * <li>Implement {@link IPathChooseListener}</li>
 * <li>Override {@link Activity#onActivityResult} where handle {@link App#REQUEST_CODE_FILE_CHOOSER} with {@link #handleFileChooserResult}</li>
 * <li>Call {@link #askFilePath} or {@link #askFolderPath}, result handle in {@link IPathChooseListener#onFileChoosed}</li>
 * </ol>
 */
public class PathChooser extends Activity {
    private static String sMessage = App.EMPTY_STRING;
    private static String sDefaultPath = App.EMPTY_STRING;
    private static String sFileType = App.EMPTY_STRING;
    private static Activity sParent;
    private static IPathChooseListener sListener;
    private static boolean isNeedFile;
    private static EditText mTextView;
    private static AlertDialog mDialog;

    /**
     * Shows dialog for choose path to file.
     * See details in {@link PathChooser} comments.
     * 
     * @param parent {@link Activity} which called it.
     * @param listener {@link IPathChooseListener} to receive result.
     * @param message Message of dialog.
     * @param defaultPath Path to set as default.
     * @param type String pattern to filter files.
     */
    public static void askFilePath(Activity parent, IPathChooseListener listener, String message, String defaultPath, String type) {
        sParent = parent;
        sListener = listener;
        sMessage = message;
        sDefaultPath = defaultPath;
        sFileType = type;
        isNeedFile = true;
        showDialog();
    }

    /**
     * Shows dialog for choose path to folder.
     * See details in {@link PathChooser} comments.
     * 
     * @param parent {@link Activity} which called it.
     * @param listener {@link IPathChooseListener} to receive result.
     * @param message Message of dialog.
     * @param defaultPath Path to set as default.
     */
    public static void askFolderPath(Activity parent, IPathChooseListener listener, String message, String defaultPath) {
        sParent = parent;
        sListener = listener;
        sMessage = message;
        sDefaultPath = defaultPath;
        isNeedFile = false;
        showDialog();
    }

    /**
     * Handled {@link PathChooser} results.
     * 
     * @param resultCode Result code received in "onActvityResult" parameters.
     * @param data Data received in "onActvityResult" parameters.
     * @return String with path or <code>null</code> if user has not choose path (still).
     */
    public static void handleFileChooserResult(int resultCode, Intent data) {
        if (resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            if (uri != null && uri.isAbsolute()) {
                mTextView.setText(uri.getPath());
            }
        }
        mDialog.show();
    }

    private static void showDialog() {
        if (mDialog != null) {
            mDialog.cancel();
        }
        mTextView = new EditText(sParent);
        mTextView.setText(sDefaultPath);
        mDialog = new AlertDialog.Builder(sParent)
                .setTitle(sMessage)
                .setView(mTextView)
                .setPositiveButton(sParent.getString(R.string.choose), browseDialogListener)
                .setNeutralButton(sParent.getString(R.string.browse), browseDialogListener)
                .setNegativeButton(sParent.getString(R.string.cancel), browseDialogListener)
                .show();
    }

    private static DialogInterface.OnClickListener browseDialogListener = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {


            switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                dialog.cancel();
                File folder = new File(mTextView.getText().toString());
                if (folder.isDirectory() || folder.mkdir()) {
                    sListener.onPathChoose(true, folder.getAbsolutePath());
                } else {
                    Toast.makeText(sParent,
                            sParent.getString(R.string.choosed_wrong_folder, folder.getAbsolutePath()),
                            Toast.LENGTH_SHORT).show();
                    sListener.onPathChoose(false, sDefaultPath);
                }
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                dialog.cancel();
                sListener.onPathChoose(false, sDefaultPath);
                break;
            case DialogInterface.BUTTON_NEUTRAL:
                sDefaultPath = mTextView.getText().toString();
                Intent intent;
                if (isNeedFile) {
                    intent = new Intent(Intent.ACTION_GET_CONTENT);
                    Uri uri = Uri.parse(sDefaultPath);
                    intent.setDataAndType(uri, sFileType);
                } else {
                    intent = new Intent(Intent.ACTION_PICK);
                    intent.setData(Uri.parse("folder://" + sDefaultPath));
                }
                PackageManager packageManager = sParent.getPackageManager();
                List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
                if (activities.size() > 0) {
                    sParent.startActivityForResult(Intent.createChooser(intent, sMessage), App.REQUEST_CODE_FILE_CHOOSER);
                } else {
                    Toast.makeText(sParent, sParent.getString(R.string.browse_error), Toast.LENGTH_LONG).show();
                }
                break;
            default:
                Toast.makeText(sParent, "wtf? which=" + which, Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
}
