package com.am.organizer.ui.helpers;

import java.util.Date;

public interface IDateChooseListener {

    /**
     * Called if user choose new date.
     * 
     * @param date Selected {@link Date}.
     */
    public void onDateChoosed(Date date);
}
