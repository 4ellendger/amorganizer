package com.am.organizer.ui.helpers;

public interface ITimeChooseListener {

    /**
     * Called if user choose new time.
     * 
     * @param hourOfDay Selected hours.
     * @param minute Selected minutes.
     */
    public void onTimeChoosed(int hourOfDay, int minute);

}
