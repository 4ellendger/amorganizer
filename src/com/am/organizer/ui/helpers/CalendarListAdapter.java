package com.am.organizer.ui.helpers;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.am.organizer.App;
import com.am.organizer.CalendarActivity;
import com.am.organizer.R;
import com.am.organizer.data.Week;
import com.am.organizer.ui.view.DayView;

/**
 * List adapter Calendar ListView. Dynamically created weeks and row views.
 */
public class CalendarListAdapter extends ArrayAdapter<Void> {
    private static CalendarActivity sParent;

    public CalendarListAdapter(Context context) {
        super(context, 0, (List<Void>) null);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) App.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_calendar, null, true);
            holder = new ViewHolder();
            holder.days = new DayView[Week.DAYS_COUNT];
            holder.days[0] = (DayView) rowView.findViewById(R.id.d0);
            holder.days[1] = (DayView) rowView.findViewById(R.id.d1);
            holder.days[2] = (DayView) rowView.findViewById(R.id.d2);
            holder.days[3] = (DayView) rowView.findViewById(R.id.d3);
            holder.days[4] = (DayView) rowView.findViewById(R.id.d4);
            holder.days[5] = (DayView) rowView.findViewById(R.id.d5);
            holder.days[6] = (DayView) rowView.findViewById(R.id.d6);
            holder.monthLabel = (TextView) rowView.findViewById(R.id.month_label);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }

        //Log.d(CalendarActivity.TAG, "CalendarListAdapter.getView, position=" + position + " (start=" + sStartWeekIndex + ")");
        Week week = Week.getWeekByCalendarIndex(position);

        // Create days views.
        int dayColorResId;
        for (int i = 0; i < Week.DAYS_COUNT; i++) {
            dayColorResId = R.color.day_view_other;
            if (position >= App.getCurrentMonthStartWeekIndex()
                    && position <= App.getCurrentMonthEndWeekIndex()) {
                if (position == App.getCurrentMonthStartWeekIndex()) {

                    // It is first week of month.
                    if (i >= App.getCurrentMonthStartWeekDayIndex()) {
                        dayColorResId = R.color.day_view_month;
                    }
                } else if (position == App.getCurrentMonthEndWeekIndex()) {

                    // It is last week of month.
                    if (i <= App.getCurrentMonthEndWeekDayIndex()) {
                        dayColorResId = R.color.day_view_month;
                    }
                } else {
                    dayColorResId = R.color.day_view_month;
                }

                // Check that today.
                if (position == App.getCurrentWeekIndex() && i == App.getCurrentDayInWeek()) {
                    dayColorResId = R.color.day_view_today;
                }
            }
            DayView cell = holder.days[i];
            cell.setTime(week.getTimeForDay(i));
            cell.setBackgroundColor(getContext().getResources().getColor(dayColorResId));
            cell.setText(String.valueOf(week.getDayNumberForDay(i)));
            cell.setOnClickListener(sParent);
            cell.setOnLongClickListener(sParent);
        }
        holder.monthLabel.setText(week.getMonthName());
        return rowView;
    }

    /**
     * Class to save same views on rows.
     */
    static class ViewHolder {
        public DayView days[];
        public TextView monthLabel;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    public static void setParent(CalendarActivity parent) {
        sParent = parent;
    }
}
