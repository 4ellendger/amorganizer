package com.am.organizer.ui.helpers;

import android.app.Activity;

/**
 * Activity which implements "refresh" method/
 */
public abstract class RefreshableActivity extends Activity {

    /**
     * Refreshed activity content. Called in {@link #onResume()}.
     */
    public abstract void refresh();

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }
}
