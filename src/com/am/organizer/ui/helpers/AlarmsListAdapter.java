package com.am.organizer.ui.helpers;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.data.Alarm;
import com.am.organizer.data.DBHelpers;
import com.am.organizer.ui.AlarmsActivity;
import com.am.organizer.ui.AlarmsActivity.AlarmsOrder;

/**
 * List adapter for Notes activity.
 */
public class AlarmsListAdapter extends ArrayAdapter<Alarm> {
    private List<Alarm> mAlarms;

    public AlarmsListAdapter(Context context, int resource, AlarmsOrder order) {
        super(context, resource);
        mAlarms = DBHelpers.getAlarms(order);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            LayoutInflater inflater = (LayoutInflater) App.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.row_alarm, null, true);
            holder = new ViewHolder();
            holder.enabled = (CheckBox) rowView.findViewById(R.id.enabled);
            holder.name = (TextView) rowView.findViewById(R.id.alarm_time);
            holder.description = (TextView) rowView.findViewById(R.id.alarm_description);
            holder.music = rowView.findViewById(R.id.music);
            holder.vibro = rowView.findViewById(R.id.vibro);
            holder.note = (Button) rowView.findViewById(R.id.alarm_note);
            holder.fade = rowView.findViewById(R.id.alarm_note_fade);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        Alarm alarm = mAlarms.get(position);

        holder.enabled.setChecked(alarm.isEnabled());
        holder.enabled.setTag(position);
        holder.enabled.setOnCheckedChangeListener(AlarmsActivity.getInstance());

        holder.name.setText(Alarm.getFormattedTimeAndDate(alarm.getTime()));
        StringBuilder description = new StringBuilder();
        boolean isDescriptionEmpty = true;
        if (alarm.getName().length() > 0) {
            if (!isDescriptionEmpty) {
                description.append(", ");
            }
            description.append('"').append(alarm.getName()).append('"');
            isDescriptionEmpty = false;
        }
        if (alarm.getPeriod().length() > 0) {
            if (!isDescriptionEmpty) {
                description.append(", ");
            }
            description.append(alarm.getPeriod());
            isDescriptionEmpty = false;
        }
        holder.description.setText(description.toString());

        if (alarm.getMusicPath().length() > 0) {
            holder.music.setVisibility(View.VISIBLE);
        }
        if (alarm.isVibro()) {
            holder.vibro.setVisibility(View.VISIBLE);
        }

        if (alarm.getNoteId() != App.EMPTY_ID) {
            holder.note.setTag(position);
            holder.note.setOnClickListener(AlarmsActivity.getInstance());
            holder.note.setEnabled(true);
        } else {
            holder.fade.setVisibility(View.VISIBLE);
            holder.note.setEnabled(false);
        }
        return rowView;
    }

    @Override
    public int getCount() {
        return mAlarms.size();
    }

    @Override
    public Alarm getItem(int pos){
        return mAlarms.get(pos);
    }

    /**
     * Class to save same views on rows.
     */
    static class ViewHolder {
        public CheckBox enabled;
        public TextView name;
        public TextView description;
        public View music;
        public View vibro;
        public Button note;
        public View fade;
    }

    public List<Alarm> getAlarms() {
        return mAlarms;
    }
}
