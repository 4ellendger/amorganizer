package com.am.organizer.ui.helpers;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.text.format.DateFormat;
import android.util.Log;

import com.am.organizer.App;

public class DialogsHelper {

    /**
     * Showed dialog with buttons and wait for result. Should be called not from UI thread.
     * 
     * @param parent {@link Activity} for which show dialog.
     * @param message Message with ask to user.
     * @param buttonTexts Texts for buttons in order:<ol>
     * <li>Negative (result - {@link DialogInterface#BUTTON_NEGATIVE})</li>
     * <li>Positive (result - {@link DialogInterface#BUTTON_POSITIVE})</li>
     * <li>Neutral (optional)(result - {@link DialogInterface#BUTTON_NEUTRAL})</li>
     * </ol>
     * @return Identified of button which user choose.
     */
    public static synchronized int askUserButton(Activity parent, String message, String[] buttonTexts) {
        ResultIntRunnable choiceRunnable = new ResultIntRunnable(parent, message, buttonTexts);
        parent.runOnUiThread(choiceRunnable);
        synchronized (choiceRunnable) {
            try {
                choiceRunnable.wait();
            } catch (InterruptedException e) {
                Log.e(App.TAG, DialogsHelper.class.getSimpleName()
                        + ".getUserChoise: Cannot wait choisen for '" + message + "'.", e);
            }
        }
        return choiceRunnable.getResult();
    }

    /**
     * Internal {@link Runnable} class which shows dialog and returns pressed button ID.
     */
    private static class ResultIntRunnable implements Runnable {
        AlertDialog.Builder dialogBuilder;
        ResultIntRunnable instance;
        int pressedButton = DialogInterface.BUTTON_NEGATIVE;
        DialogInterface.OnClickListener resultDialogListener = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                pressedButton = which;
                synchronized (instance) {
                    instance.notify();
                }
            }
        };

        ResultIntRunnable (Activity parent, String message, String[] buttonTexts) {
            dialogBuilder = new AlertDialog.Builder(parent)
            .setMessage(message);
            if (buttonTexts.length > 1) {
                dialogBuilder.setNegativeButton(buttonTexts[0], resultDialogListener)
                .setNeutralButton(buttonTexts[1], resultDialogListener);
            }
            if (buttonTexts.length == 3)
                dialogBuilder.setPositiveButton(buttonTexts[2], resultDialogListener);
        }


        public int getResult() {
            return pressedButton;
        }

        @Override
        public void run() {
            instance = this;
            dialogBuilder.show();
        }
    }

    /**
     * Shows modal window with pickers to choose time (relative to current locale).
     * 
     * @param parent {@link Activity} for which opened dialog.
     * @param listener {@link TimePickerDialog.OnTimeSetListener} to listen when user choose time.
     * @param title Title of dialog.
     * @param initialTime Initial picker value.
     */
    public static void askTime(Activity parent, TimePickerDialog.OnTimeSetListener listener,
            String title, long initialTime) {
        Calendar calendar = Calendar.getInstance(App.getCurrentLocale());
        calendar.setTimeInMillis(initialTime);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(parent, listener, hour, minute, DateFormat.is24HourFormat(parent));
        mTimePicker.setTitle(title);
        mTimePicker.show();
    }

    /**
     * Shows modal window with pickers to choose delay (in hours and minutes).
     * 
     * @param parent {@link Activity} for which opened dialog.
     * @param listener {@link TimePickerDialog.OnTimeSetListener} to listen when user choose time.
     * @param title Title of dialog.
     * @param initialTime Initial picker value.
     */
    public static void askDelay(Activity parent, TimePickerDialog.OnTimeSetListener listener,
            String title, long initialTime) {
        Calendar calendar = Calendar.getInstance(App.getCurrentLocale());
        calendar.setTimeInMillis(initialTime - calendar.getTimeZone().getOffset(initialTime));
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(parent, listener, hour, minute, DateFormat.is24HourFormat(parent));
        mTimePicker.setTitle(title);
        mTimePicker.show();
    }

    /**
     * Shows modal window with pickers to choose date.
     * 
     * @param parent {@link Activity} for which opened dialog.
     * @param listener {@link DatePickerDialog.OnDateSetListener} to listen when user choose date.
     * @param title Title of dialog.
     * @param initialDate Initial picker value.
     */
    public static void askDate(Activity parent, DatePickerDialog.OnDateSetListener listener,
            String title, long initialDate) {
        Calendar calendar = Calendar.getInstance(App.getCurrentLocale());
        calendar.setTimeInMillis(initialDate);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog mTimePicker;
        mTimePicker = new DatePickerDialog(parent, listener, year, month, day);
        mTimePicker.setTitle(title);
        mTimePicker.show();
    }
}
