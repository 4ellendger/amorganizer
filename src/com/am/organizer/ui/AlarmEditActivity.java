package com.am.organizer.ui;

import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.data.Alarm;
import com.am.organizer.data.DBHelpers;
import com.am.organizer.data.DataManager;
import com.am.organizer.data.helpers.MusicHelper;
import com.am.organizer.ui.helpers.DialogsHelper;
import com.am.organizer.ui.helpers.IPathChooseListener;
import com.am.organizer.ui.helpers.PathChooser;
import com.am.organizer.ui.helpers.UIHelpers;

public class AlarmEditActivity extends Activity implements DatePickerDialog.OnDateSetListener,
        TimePickerDialog.OnTimeSetListener, IPathChooseListener {

    public static final String AUTO_NAME_FORMAT = "HH:mm EEE MM-dd";

    private static AlarmEditActivity sInstance;
    private Alarm mAlarm;

    private boolean isChanged = true;
    private boolean isSaved = true;
    private boolean isNameAuto = true;
    private boolean isWaitBother = false;
    private boolean isPressBackAfterSave = false;

    private TextView mTvName;
    private CheckBox mChEnabled;
    private TextView mTvDate;
    private TextView mTvTime;
    private CheckBox mChDeleteOnHappen;
    private TextView mTvMusic;
    private Button mBPlay;
    private CheckBox mChVibro;
    private TextView mTvPeriod;
    private TextView mTvBother;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize not UI variables.
        sInstance = this;
        long alarmId = getIntent().getLongExtra(App.EXTRA_ALARM_ID, App.EMPTY_ID);
        long noteId = getIntent().getLongExtra(App.EXTRA_NOTE_ID, App.EMPTY_ID);
        long mFromTime = getIntent().getLongExtra(App.EXTRA_TIME, App.EMPTY_TIME);
        String name = getIntent().getStringExtra(App.EXTRA_NAME);

        // Find all used widgets.
        setContentView(R.layout.activity_alarm_edit);
        mTvName = (TextView) findViewById(R.id.l_name_value);
        mChEnabled = (CheckBox) findViewById(R.id.cb_enabled);
        mTvDate = (TextView) findViewById(R.id.l_date_value);
        mTvTime = (TextView) findViewById(R.id.l_time_value);
        mChDeleteOnHappen = (CheckBox) findViewById(R.id.cb_delete_on_happen);
        mTvMusic = (TextView) findViewById(R.id.l_music_value);
        mBPlay = (Button) findViewById(R.id.b_play);
        mChVibro = (CheckBox) findViewById(R.id.cb_vibro);
        mTvPeriod = (TextView) findViewById(R.id.l_period_value);
        mTvBother = (TextView) findViewById(R.id.l_bother_value);

        // Create mAlarm if not specified.
        if (alarmId == App.EMPTY_ID) {
            mAlarm = new Alarm();
            mAlarm.setNoteId(noteId);
            mAlarm.setEnabled(true);
            if (mFromTime == App.EMPTY_TIME) {
                mFromTime = Calendar.getInstance(App.getCurrentLocale()).getTimeInMillis() + App.getTimeDelay();
            }
            mAlarm.setTime(mFromTime);
            if (name != null && name.length() > 0) {
                mTvName.setText(name);
                isNameAuto = false;
            } else {
                isNameAuto = true;
            }
            mAlarm.setDeleteOnEvent(App.isDeleteOnHappen());
            mAlarm.setMusicPath(App.getAlarmMusicPath());
            mAlarm.setVibro(App.isVibroEnabled());
            mAlarm.setPeriod(App.getAlarmPeriod());
            mAlarm.setBother(App.getBother());
            isChanged = true;
            isSaved = false;
        } else {
            mAlarm = DBHelpers.getAlarm(alarmId);
            if (mAlarm == null) {
                throw new NullPointerException("Cannot get alarm with " + alarmId + " ID.");
            }
            isNameAuto = false;
            isChanged = false;
            isSaved = true;
        }

        // Full widgets from Alarm object.
        mTvName.setText(mAlarm.getName());
        mChEnabled.setChecked(mAlarm.isEnabled());
        updateTimeAndName();
        mChDeleteOnHappen.setChecked(mAlarm.isDeleteOnHappen());
        mTvMusic.setText(mAlarm.getMusicPath());
        mChVibro.setChecked(mAlarm.isVibro());
        mTvPeriod.setText(mAlarm.getPeriod());
        mTvBother.setText(mAlarm.getFormattedBother());
        isNameAuto = false;
        isChanged = false;
        isSaved = true;

        // Set values and listeners of widgets.
        mTvName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // If user remove name then stop to change name automatically.
                if (s.length() == 0) {
                    isNameAuto = false;
                }
                isChanged = true;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void afterTextChanged(Editable s) { }
        });
        OnCheckedChangeListener checkedListener = new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isChanged = true;
            }
        };
        mChEnabled.setOnCheckedChangeListener(checkedListener);
        mChDeleteOnHappen.setOnCheckedChangeListener(checkedListener);
        mChVibro.setOnCheckedChangeListener(checkedListener);
        mTvDate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogsHelper.askDate(sInstance, sInstance, getString(R.string.choose_date), mAlarm.getTime());
            }
        });
        mTvTime.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                isWaitBother = false;
                DialogsHelper.askTime(sInstance, sInstance, getString(R.string.choose_time), mAlarm.getTime());
            }
        });
        mTvMusic.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                PathChooser.askFilePath(sInstance, sInstance,
                        getString(R.string.choose_music),
                        App.getAlarmMusicPath(),
                        App.FILE_EXT_FILTER_FOR_MUSIC);
            }
        });
        mBPlay.setOnClickListener(new OnClickListener() {
            boolean isPlayed = false;

            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(mAlarm.getMusicPath())) {
                    if (!isPlayed) {
                        MusicHelper.startPlay(mAlarm.getMusicPath());
                        v.setBackgroundResource(R.drawable.stop);
                    } else {
                        MusicHelper.stopPlay();
                        v.setBackgroundResource(R.drawable.play);
                    }
                    isPlayed = !isPlayed;
                }
            }
        });
        mTvBother.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                isWaitBother = true;
                DialogsHelper.askDelay(sInstance, sInstance, getString(R.string.choose_bother), mAlarm.getBother());
            }
        });
        mTvPeriod.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isChanged = true;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void afterTextChanged(Editable s) { }
        });
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_alarm_menu, menu);
        UIHelpers.scaleMenuItems(this, menu, new int[] { R.id.action_save, R.id.action_note });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mTvName.getText().length() == 0) {
            isNameAuto = true;
        }
    }

    @Override
    public void onBackPressed () {
        if (confirmLeave()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            onBackPressed();
            return true;
        case R.id.action_save:
            updateAlarmFromUi();
            showAskSaveDialog(false);
            break;
        case R.id.action_note:
            if (confirmLeave()) {
                Intent intent = new Intent(sInstance, NoteEditActivity.class);
                if (mAlarm.getNoteId() != App.EMPTY_ID) {
                    intent.putExtra(App.EXTRA_NOTE_ID, mAlarm.getNoteId());
                } else {
                    intent.putExtra(App.EXTRA_TIME, mAlarm.getTime());
                }
                if (mAlarm.getId() != App.EMPTY_ID) { 
                    intent.putExtra(App.EXTRA_ALARM_ID, mAlarm.getId());
                }
                startActivity(intent);
            }
            break;
        case R.id.help:
            if (confirmLeave()) {
                UIHelpers.showHelp(this);
            }
            break;
        case R.id.settings:
            if (confirmLeave()) {
                UIHelpers.showSettings(this);
            }
            break;
        default:
            Toast.makeText(this, "wtf? id=" + item.getItemId(), Toast.LENGTH_SHORT).show();
            break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == App.REQUEST_CODE_FILE_CHOOSER) {
            PathChooser.handleFileChooserResult(resultCode, data);
        }
    }

    /**
     * Updated 'Date', 'Time' and 'Name' (if isNameAuto) text fields.
     */
    private void updateTimeAndName() {
        mTvDate.setText(mAlarm.getFormattedDate());
        mTvTime.setText(mAlarm.getFormattedTime());
        if (isNameAuto) {
            mTvName.setText(mAlarm.getFormattedTime());
        }
        isChanged = true;
    }

    /**
     * Checks that user can leave this activity without loosing data.
     * 
     * @return <code>true</code> if can leave, <code>false</code> otherwise.
     */
    private boolean confirmLeave() {
        updateAlarmFromUi();
        if (isChanged || !isSaved) {
            if (mAlarm.isEnabled()) {

                // If enabled then save in any case.
                DataManager.saveAlarm(this, mAlarm);
            } else {
                showAskSaveDialog(true);
                return false;
            }
        }
        return true;
    }

    /**
     * Shows "Save" dialog.
     * 
     * @param isPressBackAfterSaveflag that need emulate "Back" button press if user confirm exit.
     */
    private void showAskSaveDialog(boolean isPressBackAfterSave) {
        this.isPressBackAfterSave = isPressBackAfterSave;
        new AlertDialog.Builder(this)
        .setMessage(getString(R.string.alarm_ask_save, mAlarm.getName()))
        .setNegativeButton(getString(R.string.no), askSaveDialogListener)
        .setNeutralButton(getString(R.string.save), askSaveDialogListener)
        .setPositiveButton(getString(R.string.save_and_enable), askSaveDialogListener)
        .show();
    }

    /**
     * Updated mAlarm fields from widgets.
     */
    private void updateAlarmFromUi() {
        if (isChanged) {

            // Time variable stored in mAlarm - not need to update.
            mAlarm.setName(mTvName.getText().toString());
            mAlarm.setEnabled(mChEnabled.isChecked());
            mAlarm.setDeleteOnEvent(mChDeleteOnHappen.isChecked());
            mAlarm.setMusicPath(mTvMusic.getText().toString());
            mAlarm.setVibro(mChVibro.isChecked());
            mAlarm.setPeriod(mTvPeriod.getText().toString());
            isChanged = false;
        }
    }

    /**
     * Handler of clicks on "Would you save alarm" dialog.
     */
    DialogInterface.OnClickListener askSaveDialogListener = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                mAlarm.setEnabled(true);
                mChEnabled.setChecked(true);
            case DialogInterface.BUTTON_NEUTRAL:
                DataManager.saveAlarm(sInstance, mAlarm);
                break;
            default:

                // If after dialog need to leave activity then make confirmLeave return true;
                if (isPressBackAfterSave) {
                    isChanged = false;
                    isSaved = true;
                }
                break;
            }
            dialog.dismiss();
            if (isPressBackAfterSave) {
                sInstance.onBackPressed();
            }
        }
    };

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Calendar c = Calendar.getInstance(App.getCurrentLocale());
        if (!isWaitBother) {
            c.setTimeInMillis(mAlarm.getTime());
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            c.setTime(new Date(App.EMPTY_TIME));
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, month);
            c.set(Calendar.DAY_OF_MONTH, day);
            c.set(Calendar.HOUR_OF_DAY, hourOfDay);
            c.set(Calendar.MINUTE, minute);
            mAlarm.setTime(c.getTimeInMillis());
            updateTimeAndName();
        } else {
            c.setTime(new Date(App.EMPTY_TIME));
            c.set(Calendar.HOUR_OF_DAY, hourOfDay);
            c.set(Calendar.MINUTE, minute);
            mAlarm.setBother(c.getTimeInMillis() + c.getTimeZone().getOffset(c.getTimeInMillis()));
            mTvBother.setText(mAlarm.getFormattedBother());
            isChanged = true;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar c = Calendar.getInstance(App.getCurrentLocale());
        c.setTimeInMillis(mAlarm.getTime());
        int hourOfDay = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        c.setTime(new Date(App.EMPTY_TIME));
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, monthOfYear);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        c.set(Calendar.HOUR_OF_DAY, hourOfDay);
        c.set(Calendar.MINUTE, minute);
        mAlarm.setTime(c.getTimeInMillis());
        updateTimeAndName();
    }

    @Override
    public void onPathChoose(boolean isChoosed, String path) {
        if (isChoosed) {
            mTvMusic.setText(path);
            isChanged = true;
        }
    }
}
