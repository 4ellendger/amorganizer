package com.am.organizer.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * View of day in calendar.
 */
public class DayView extends TextView {
    private Paint paint = new Paint();
    private long time;

    public DayView(Context context) {
        super(context);
    }

    public DayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DayView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        super.getPaint();
        paint.setColor(Color.GRAY);
        paint.setStrokeWidth(2);
        canvas.drawLine(0, 1, getWidth(), 1, paint);
        canvas.drawLine(getWidth(), 0, getWidth(), getHeight(), paint);
        canvas.drawLine(0, getHeight() - 1, getWidth(), getHeight() - 1, paint);
        canvas.drawLine(0, 0, 0, getHeight(), paint);
    }

    public void setTemporaryBackgroundColor(int color) {
        setBackgroundColor(color);
        this.invalidate();
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getTime() {
        return time;
    }
}
