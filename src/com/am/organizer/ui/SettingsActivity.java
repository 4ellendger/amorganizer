package com.am.organizer.ui;

import java.util.Calendar;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.TimePicker;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.data.Alarm;
import com.am.organizer.data.DBHelpers;
import com.am.organizer.data.helpers.Preferences;
import com.am.organizer.ui.helpers.DialogsHelper;
import com.am.organizer.ui.helpers.IPathChooseListener;
import com.am.organizer.ui.helpers.PathChooser;

/**
 * Settings screen activity and preferences handler.
 */
public class SettingsActivity extends PreferenceActivity implements IPathChooseListener {
    public static SettingsActivity sInstance;
    public SettingsFragment mFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sInstance = this;
        mFragment = new SettingsFragment();
        getFragmentManager().beginTransaction().replace(android.R.id.content, mFragment).commit();
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void onPause() {
        super.onPause();
        Preferences.updateConfiguration(this);
    }

    @Override
    public void onPathChoose(boolean isChoosed, String path) {
        if (isChoosed && !TextUtils.isEmpty(path)) {
            Preferences.setSyncFolderPath(this, path);
            //mFragment.chooseFolder.setSummary(path); //TODO check that listener works.
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == App.REQUEST_CODE_FILE_CHOOSER) {
            PathChooser.handleFileChooserResult(resultCode, data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            onBackPressed();
            return true;
        default:
            Toast.makeText(this, "wtf? id=" + item.getItemId(), Toast.LENGTH_SHORT).show();
            break;
        }
        return true;
    }

    /**
     * Settings UI fragment.
     */
    public static class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener,
            OnClickListener, TimePickerDialog.OnTimeSetListener {
        private SettingsFragment sFragmentInstance;
        private boolean isWaitTimeDelay = true;
        private Preference chooseFolder;
        private Preference dropTables;
        private ListPreference notesOrder;
        private ListPreference alarmsOrder;
        private Preference timeDelay;
        private Preference music;
        private EditTextPreference period;
        private Preference bother;

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            sFragmentInstance = this;
            addPreferencesFromResource(R.xml.settings);

            // Find all used widgets.
            chooseFolder = (Preference) findPreference(getString(R.string.pref_sync_folder));
            dropTables = (Preference) findPreference(getString(R.string.pref_drop_tables));
            notesOrder = (ListPreference) findPreference(getString(R.string.pref_choose_notes_order));
            alarmsOrder = (ListPreference) findPreference(getString(R.string.pref_choose_alarms_order));
            timeDelay = (Preference) findPreference(getString(R.string.pref_time_delay));
            music = (Preference) findPreference(getString(R.string.pref_music));
            period = (EditTextPreference) findPreference(getString(R.string.pref_period));
            bother = (Preference) findPreference(getString(R.string.pref_bother));

            // Set "Choose sync folder" listener.
            chooseFolder.setOnPreferenceClickListener(new OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    PathChooser.askFolderPath(sInstance,
                            sInstance,
                            getString(R.string.choose_sync_folder),
                            Preferences.getSyncFolderPath(sInstance));
                    return true;
                }
            });

            // Set "Drop tables" listener.
            dropTables.setOnPreferenceClickListener(new OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    new AlertDialog.Builder(sInstance)
                    .setMessage(sInstance.getString(R.string.drop_confirmation))
                    .setPositiveButton(sInstance.getString(R.string.remove), sFragmentInstance)
                    .setNegativeButton(sInstance.getString(R.string.cancel), null)
                    .show();
                    return true;
                }
            });

            // Set "alarm time delay" listener.
            timeDelay.setOnPreferenceClickListener(new OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    isWaitTimeDelay = true;
                    DialogsHelper.askDelay(sInstance, sFragmentInstance,
                            getString(R.string.default_time_delay),
                            Preferences.getAlarmTimeDelay(sInstance));
                    return true;
                }
            });

            // Set "remind delay" listener.
            bother.setOnPreferenceClickListener(new OnPreferenceClickListener() {

                @Override
                public boolean onPreferenceClick(Preference preference) {
                    isWaitTimeDelay = false;
                    DialogsHelper.askDelay(sInstance, sFragmentInstance,
                            getString(R.string.bother), Preferences.getBother(sInstance));
                    return true;
                }
            });

            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            onSharedPreferenceChanged(prefs, App.EMPTY_STRING);// Update custom widgets.
            prefs.registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            //Log.d(App.TAG, "onSharedPreferenceChanged: key=" + key);
            chooseFolder.setSummary(Preferences.getSyncFolderPath(sInstance));
            notesOrder.setSummary(sInstance.getResources().getStringArray(R.array.notes_order_values)[Preferences.getNotesOrder(sInstance)]);
            alarmsOrder.setSummary(sInstance.getResources().getStringArray(R.array.alarms_order_values)[Preferences.getAlarmsOrder(sInstance)]);
            timeDelay.setSummary(Alarm.getFormattedDelay(Preferences.getAlarmTimeDelay(sInstance)));
            String musicPath = Preferences.getAlarmMusicPath(sInstance);
            if (musicPath == App.EMPTY_STRING) {
                music.setSummary(getString(R.string.tap_to_choose_file));
            } else {
                music.setSummary(musicPath);
            }
            period.setSummary(Preferences.getPeriod(sInstance));
            bother.setSummary(Alarm.getFormattedDelay(Preferences.getBother(sInstance)));
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(0L);
            c.set(Calendar.HOUR_OF_DAY, hourOfDay);
            c.set(Calendar.MINUTE, minute);
            if (isWaitTimeDelay) {
                Preferences.setAlarmTimeDelay(sInstance, c.getTimeInMillis() + c.getTimeZone().getOffset(c.getTimeInMillis()));
            } else {
                Preferences.setBother(sInstance, c.getTimeInMillis() + c.getTimeZone().getOffset(c.getTimeInMillis()));
            }

        }

        /**
         * Click listener for "drop data".// TODO check that really need in it method.
         */
        @Override
        public void onClick(DialogInterface dialog, int which) {
            DBHelpers.clearTables();
            if (NotesActivity.getInstance() != null) {
                NotesActivity.getInstance().refresh();
            }
            if (AlarmsActivity.getInstance() != null) {
                AlarmsActivity.getInstance().refresh();
            }
        }
    }
}
