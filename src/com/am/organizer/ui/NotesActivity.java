package com.am.organizer.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.data.DataManager;
import com.am.organizer.data.Note;
import com.am.organizer.data.helpers.FileSystemHelper;
import com.am.organizer.ui.helpers.IPathChooseListener;
import com.am.organizer.ui.helpers.NotesListAdapter;
import com.am.organizer.ui.helpers.PathChooser;
import com.am.organizer.ui.helpers.RefreshableActivity;
import com.am.organizer.ui.helpers.UIHelpers;

public class NotesActivity extends RefreshableActivity implements IPathChooseListener, OnItemClickListener, OnItemLongClickListener, OnClickListener {
    private NotesListAdapter mAdapter;
    private ListView mListView;
    private static NotesActivity sInstance;
    private NotesOrder mOrder;

    /**
     * Notes ordering variants.
     */
    public enum NotesOrder {
        BY_NAME_A,
        BY_NAME_Z,
        BY_TIME_NEW,
        BY_TIME_OLD;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sInstance = this;
        mOrder = App.getNotesOrder();
        setContentView(R.layout.acitivity_notes);
        mListView = (ListView) findViewById(R.id.list);
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void refresh() {
        sInstance.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                mAdapter = new NotesListAdapter(getBaseContext(), 0, mOrder);
                mListView.setAdapter(mAdapter);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notes_menu, menu);
        UIHelpers.scaleMenuItems(this, menu, new int[] { R.id.action_import, R.id.action_export });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            onBackPressed();
            return true;
        case R.id.action_export:
            Toast.makeText(this, "action_export TODO", Toast.LENGTH_SHORT).show();
            break;
        case R.id.action_import:
            PathChooser.askFolderPath(NotesActivity.this, NotesActivity.this, getString(R.string.choose_folder_to_import), App.getSyncFolderPath());
            break;
        case R.id.action_order_by:
            new AlertDialog.Builder(this)
            .setSingleChoiceItems(
                    getResources().getStringArray(R.array.notes_order_values),
                    mOrder.ordinal(),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mOrder = NotesOrder.values()[which];
                            refresh();
                            dialog.dismiss();
                        }
                    }).show();
            break;
        case R.id.action_refresh:
            refresh();
            break;
        case R.id.help:
            UIHelpers.showHelp(this);
            break;
        case R.id.settings:
            UIHelpers.showSettings(this);
            break;
        default:
            Toast.makeText(this, "wtf? id=" + item.getItemId(), Toast.LENGTH_SHORT).show();
            break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == App.REQUEST_CODE_FILE_CHOOSER) {
            PathChooser.handleFileChooserResult(resultCode, data);
        }
    }

    @Override
    public void onPathChoose(boolean isChoosed, final String path) {
        if (isChoosed) {
            FileSystemHelper.importNotes(this, path);
            refresh();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        Intent intent = new Intent(sInstance, NoteEditActivity.class);
        intent.putExtra(App.EXTRA_NOTE_ID, mAdapter.getItem(arg2).getId());
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        final Note note = mAdapter.getItem(arg2);
        new AlertDialog.Builder(sInstance)
        .setMessage(getString(R.string.ask_remove_note, note.getName()))
        .setNegativeButton(getString(R.string.cancel), null)
        .setPositiveButton(getString(R.string.remove), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                DataManager.removeNote(sInstance, note);
                refresh();
            }
        })
        .show();
        return true;
    }

    @Override
    public void onClick(View view) {

        // Handle click on alarm icon.
        Note note = mAdapter.getItem(((Integer) view.getTag()));
        Intent intent = new Intent(this, AlarmEditActivity.class);
        intent.putExtra(App.EXTRA_ALARM_ID, note.getAlarmId());
        intent.putExtra(App.EXTRA_NOTE_ID, note.getId());
        intent.putExtra(App.EXTRA_NAME, note.getName());
        startActivity(intent);
    }

    public static NotesActivity getInstance() {
        return sInstance;
    }
}
