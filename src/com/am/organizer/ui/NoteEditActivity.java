package com.am.organizer.ui;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.data.DBHelpers;
import com.am.organizer.data.DataManager;
import com.am.organizer.data.Note;
import com.am.organizer.ui.helpers.UIHelpers;

public class NoteEditActivity extends Activity {
    private Note mNote;
    private EditText mEditText;
    private EditText mAskNameInput;
    private boolean isTextChanged = false;
    private boolean isAskNameCalledFromOnBackPresssed = false;
    private long time = App.EMPTY_TIME;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        time = getIntent().getLongExtra(App.EXTRA_TIME, App.EMPTY_TIME);
        long noteId = getIntent().getLongExtra(App.EXTRA_NOTE_ID, App.EMPTY_ID);
        long alarmId = getIntent().getLongExtra(App.EXTRA_ALARM_ID, App.EMPTY_ID);
        String name = getIntent().getStringExtra(App.EXTRA_NAME);

        setContentView(R.layout.activity_edit_note);
        mEditText = (EditText) findViewById(R.id.edit_text);
        mEditText.setMaxLines(Integer.MAX_VALUE);

        if (noteId != App.EMPTY_ID) {
            mNote = DBHelpers.getNote(noteId);
            if (mNote == null) {
                throw new NullPointerException("Cannot get note with " + noteId + " ID.");
            }
            mEditText.setText(mNote.getText());
        } else {
            if (App.isSetDateInNewNote()) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd: ", App.getCurrentLocale());
                mEditText.setText(formatter.format(new Date()));
            }
            if (TextUtils.isEmpty(name)) {
                name = buildNoteName(System.currentTimeMillis());
            }
            mNote = new Note(alarmId, mEditText.getText().toString(), name);
        }
        mEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isTextChanged = true;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void afterTextChanged(Editable s) { }
        });
        mEditText.setSelection(mEditText.getText().length());// Move cursor in end.
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_note_menu, menu);
        UIHelpers.scaleMenuItems(this, menu, new int[] { R.id.action_save, R.id.action_alarm });
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * If need asks user confirm that all data on this activity are saved.
     * 
     * @return <code>true</code> if can leave, <code>false</code> otherwise.
     */
    private boolean confirmLeave() {
        if (isTextChanged) {
            updateNoteTextAndName();
            new AlertDialog.Builder(this)
            .setMessage(getString(R.string.note_ask_save, mNote.getName()))
            .setNegativeButton(getString(R.string.no), askSaveDialogListener)
            .setNeutralButton(getString(R.string.cancel), askSaveDialogListener)
            .setPositiveButton(getString(R.string.save), askSaveDialogListener)
            .show();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed () {
        if (confirmLeave()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            onBackPressed();
            return true;
        case R.id.choose_name:
            updateNoteTextAndName();
            askSetName(false);
            break;
        case R.id.action_save:
            updateNoteTextAndName();
            DataManager.saveNote(this, mNote);
            break;
        case R.id.action_alarm:
            if (confirmLeave()) {
                Intent intent = new Intent(NoteEditActivity.this, AlarmEditActivity.class);
                if (mNote.getId() != App.EMPTY_ID) {
                    intent.putExtra(App.EXTRA_NOTE_ID, mNote.getId());
                    if (time != App.EMPTY_TIME) {
                        intent.putExtra(App.EXTRA_TIME, time);
                    }
                }
                if (mNote.getAlarmId() != App.EMPTY_ID) { 
                    intent.putExtra(App.EXTRA_ALARM_ID, mNote.getAlarmId());
                }
                startActivity(intent);
            }
            break;
        case R.id.help:
            if (confirmLeave()) {
                UIHelpers.showHelp(this);
            }
            break;
        case R.id.settings:
            if (confirmLeave()) {
                UIHelpers.showSettings(this);
            }
            break;
        default:
            Toast.makeText(this, "wtf? id=" + item.getItemId(), Toast.LENGTH_SHORT).show();
            break;
        }
        return true;
    }

    /**
     * Updated mNote text and name from widgets.
     */
    private void updateNoteTextAndName () {
        if (isTextChanged) {
            mNote.setText(mEditText.getText().toString());
        }
        if (TextUtils.isEmpty(mNote.getName())) {
            mNote.setName(buildNoteName(mNote.getUpdateTime()));
        }
    }

    private String buildNoteName(long time) {
        String noteText = mNote.getText();
        if (noteText.length() > Note.NAME_FROM_TEXT_LENGTH) {
            return noteText.substring(0, Note.NAME_FROM_TEXT_LENGTH);
        } else if (noteText.length() > 0) {
            return noteText;
        } else {
            return Note.getFormattedTime(time);
        }
    }

    /**
     * Shows dialog with asking to set name.
     * 
     * @param isFromOnBackPressed Flag that called from "onBackPressed".
     */
    private void askSetName(boolean isFromOnBackPressed) {
        this.isAskNameCalledFromOnBackPresssed = isFromOnBackPressed;
        mAskNameInput = new EditText(this);
        mAskNameInput.setText(mNote.getName());
        new AlertDialog.Builder(this)
        .setTitle(getString(R.string.choose_name))
        .setView(mAskNameInput)
        .setPositiveButton(getString(R.string.save), askNameDialogListener)
        .setNegativeButton(getString(R.string.cancel), askNameDialogListener)
        .show();
    }

    /**
     * Handler of clicks on "Would you save note" dialog.
     */
    DialogInterface.OnClickListener askSaveDialogListener = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                askSetName(true);
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                isTextChanged = false;
                NoteEditActivity.this.onBackPressed();
                break;
            default:
                break;
            }
            dialog.dismiss();
        }
    };

    /**
     * Handler of clicks on "Set note name" dialog.
     */
    DialogInterface.OnClickListener askNameDialogListener = new DialogInterface.OnClickListener() {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                if (mAskNameInput != null && mAskNameInput.getText().length() > 0) {
                    mNote.setName(mAskNameInput.getText().toString());
                    if (isAskNameCalledFromOnBackPresssed) {
                        DataManager.saveNote(NoteEditActivity.this, mNote);
                        NoteEditActivity.this.onBackPressed();
                    }
                }
            } else {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            }
        }
    };
}
