package com.am.organizer.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Toast;

import com.am.organizer.App;
import com.am.organizer.R;
import com.am.organizer.alarm.AlarmManagerHelper;
import com.am.organizer.data.Alarm;
import com.am.organizer.data.DBHelpers;
import com.am.organizer.data.DataManager;
import com.am.organizer.ui.helpers.AlarmsListAdapter;
import com.am.organizer.ui.helpers.UIHelpers;

public class AlarmsActivity extends Activity implements OnItemClickListener,
        OnItemLongClickListener, OnCheckedChangeListener, OnClickListener {
    private ListView mListView;
    private static AlarmsActivity sInstance;
    private AlarmsOrder mOrder;
    private AlarmsListAdapter mAdapter;

    public enum AlarmsOrder {
        BY_TIME_NEW,
        BY_TIME_OLD,
        FIRST_PERIODIC_BY_TIME_NEW,
        BY_NOTE_NAME_A,
        BY_NOTE_NAME_Z;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sInstance = this;
        mOrder = App.getAlarmsOrder();
        setContentView(R.layout.acitivity_notes);
        mListView = (ListView) findViewById(R.id.list);
        mListView.setOnItemClickListener(this);
        mListView.setOnItemLongClickListener(this);
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        refresh();
    }

    public void refresh() {
        sInstance.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                mAdapter = new AlarmsListAdapter(getBaseContext(), 0, mOrder);
                mListView.setAdapter(mAdapter);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.alarms_menu, menu);
        UIHelpers.scaleMenuItems(this, menu, new int[] { R.id.action_disable_all_alarms, R.id.action_remove_all_alarms });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            onBackPressed();
            return true;
        case R.id.action_disable_all_alarms:
            if (mAdapter.getCount() > 0) {
                new AlertDialog.Builder(sInstance)
                .setMessage(getString(R.string.ask_disable_all_alarms))
                .setNegativeButton(getString(R.string.cancel), null)
                .setPositiveButton(getString(R.string.disable), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        
                        DBHelpers.logTable(DBHelpers.TABLE_ALARMS);
                        
                        for (Alarm alarm : mAdapter.getAlarms()) {
                            if (alarm.isEnabled()) {
                                alarm.setEnabled(false);
                                DBHelpers.updateAlarm(alarm);
                                AlarmManagerHelper.cancelAlarmById(sInstance, alarm.getId());
                            }
                        }
                        
                        DBHelpers.logTable(DBHelpers.TABLE_ALARMS);
                        
                        refresh();
                    }
                })
                .show();
            }
            break;
        case R.id.action_remove_all_alarms:
            if (mAdapter.getCount() > 0) {
                new AlertDialog.Builder(sInstance)
                .setMessage(getString(R.string.ask_remove_all_alarms))
                .setNegativeButton(getString(R.string.cancel), null)
                .setPositiveButton(getString(R.string.remove), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DataManager.removeAllAlarms(sInstance);
                        refresh();
                    }
                })
                .show();
            }
            break;
        case R.id.action_order_by:
            new AlertDialog.Builder(this)
            .setSingleChoiceItems(
                    getResources().getStringArray(R.array.alarms_order_values),
                    mOrder.ordinal(),
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mOrder = AlarmsOrder.values()[which];
                            refresh();
                            dialog.dismiss();
                        }
                    }).show();
            break;
        case R.id.action_refresh:
            refresh();
            break;
        case R.id.help:
            UIHelpers.showHelp(this);
            break;
        case R.id.settings:
            UIHelpers.showSettings(this);
            break;
        default:
            Toast.makeText(this, "wtf? id=" + item.getItemId(), Toast.LENGTH_SHORT).show();
            break;
        }
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        Intent intent = new Intent(sInstance, AlarmEditActivity.class);
        intent.putExtra(App.EXTRA_ALARM_ID, mAdapter.getItem(arg2).getId());
        startActivity(intent);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
        final Alarm alarm = mAdapter.getItem(arg2);
        new AlertDialog.Builder(sInstance)
        .setMessage(getString(R.string.ask_remove_alarm, alarm.getName()))
        .setNegativeButton(getString(R.string.cancel), null)
        .setPositiveButton(getString(R.string.remove), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (DBHelpers.removeAlarm(alarm.getId())) {
                    Toast.makeText(sInstance, getString(R.string.alarm_removed_toast, alarm.getName()), Toast.LENGTH_SHORT).show();
                    refresh();
                }
            }
        })
        .show();
        return true;
    }

    public static AlarmsActivity getInstance() {
        return sInstance;
    }

    @Override
    public synchronized void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        // Handling click on "enabled" check box.
        Alarm alarm = (Alarm) mAdapter.getItem(((Integer) buttonView.getTag()));
        alarm.setEnabled(isChecked);
        DBHelpers.updateAlarm(alarm);
        AlarmManagerHelper.syncAlarm(this, alarm);
    }

    @Override
    public void onClick(View view) {

        // Handling click on note icon.
        Alarm alarm = (Alarm) mAdapter.getItem(((Integer) view.getTag()));
        Intent intent = new Intent(this, NoteEditActivity.class);
        intent.putExtra(App.EXTRA_NOTE_ID, alarm.getNoteId());
        intent.putExtra(App.EXTRA_ALARM_ID, alarm.getId());
        intent.putExtra(App.EXTRA_NAME, alarm.getName());
        startActivity(intent);
    }
}
