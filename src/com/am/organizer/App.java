package com.am.organizer;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.content.res.Resources;

import com.am.organizer.data.helpers.DBHelper;
import com.am.organizer.data.helpers.Preferences;
import com.am.organizer.ui.AlarmsActivity.AlarmsOrder;
import com.am.organizer.ui.NotesActivity.NotesOrder;

/**
 * Global application resources storage.
 */
public class App {

    // Constants for empty values.
    public static final String EMPTY_STRING = "";
    public static final long EMPTY_ID = -1L;
    public static final long EMPTY_TIME = 0;

    // Bundle extra keys.
    public static final String EXTRA_TIME = "TIME";
    public static final String EXTRA_NOTE_ID = "NOTE_ID";
    public static final String EXTRA_ALARM_ID = "ALARM_ID";
    public static final String EXTRA_PATH = "PATH";
    public static final String EXTRA_NAME = "NAME";

    // Activity result codes.
    public static final int REQUEST_CODE_GET_FILE_PATH = 100;
    public static final int REQUEST_CODE_GET_FOLDER_PATH = 101;
    public static final int REQUEST_CODE_FILE_CHOOSER = 102;

    // Other constants.
    public static final int START_WEEK_FROM_EPOCH = 2000;
    public static final String TAG = "AMOrganizer";
    public static final int MAX_NOTE_BYTES_LENGHT = 15 * 1024;
    public static final String FILE_EXT_FILTER_FOR_MUSIC = "*.mp3";
    public static final TimeZone GREENWICH_TIMEZONE= TimeZone.getTimeZone("GMT");
    public static final Locale GREENWICH_LOCALE = Locale.UK;

    private static Context sContext;
    private static DBHelper sDbHelper;
    private static int sCurrentWeekIndex = 0;
    private static int sCurrentDayInWeek = 0;
    private static int sCurrentMonthStartWeekIndex = 0;
    private static int sCurrentMonthStartWeekDayIndex = 0;
    private static int sCurrentMonthEndWeekIndex = 0;
    private static int sCurrentMonthEndWeekDayIndex = 0;

    private static boolean sSyncWithFolder;
    private static String sSyncFolderPath;
    private static NotesOrder sNotesOrder;
    private static boolean sIsSetDateInNewNote;
    private static AlarmsOrder sAlarmsOrder;
    private static long sTimeDelay;
    private static boolean sIsDeleteOnHappen;
    private static String sAlarmMusicPath;
    private static boolean sIsVibroEnabled;
    private static String sPeriod;
    private static long sBother;

    public static void init(Context context) {
        sContext = context;
        updateCurrentDate();
        Preferences.updateConfiguration(context);
    }

    public static Context getContext() {
        return sContext;
    }

    public static Resources getResources() {
        return sContext.getResources();
    }

    /**
     * Calculated all data to show calendar.
     */
    public static void updateCurrentDate() {
        Calendar calendar = Calendar.getInstance();// Calendar for now.

        // Save current year, month and day of month.
        long currentYear = calendar.get(Calendar.YEAR);
        long currentMonth = calendar.get(Calendar.MONTH);
        long currentDay = calendar.get(Calendar.DAY_OF_MONTH);

        // Set calendar to epoch + START_WEEK_FROM_EPOCH.
        calendar.setTimeInMillis(0);
        calendar.add(Calendar.WEEK_OF_YEAR, START_WEEK_FROM_EPOCH);

        // Start to add days until find current month.
        int week = 0;
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - calendar.getFirstDayOfWeek();
        boolean isMonthFound = false;
        boolean isNextMonthStarted = false;
        while (true) {

            // Set flag to handle next month.
            isNextMonthStarted = isMonthFound;

            if (calendar.get(Calendar.YEAR) == currentYear) {
                if (calendar.get(Calendar.MONTH) == currentMonth) {
                    isNextMonthStarted = false;// Set false to handle next month.

                    // Check that it first day of month.
                    if (!isMonthFound) {
                        sCurrentMonthStartWeekDayIndex= dayOfWeek;
                        sCurrentMonthStartWeekIndex = week;
                        isMonthFound = true;
                    }

                    // Check that it current day.
                    if (calendar.get(Calendar.DAY_OF_MONTH) == currentDay) {
                        sCurrentWeekIndex = week;
                        sCurrentDayInWeek = dayOfWeek;
                    }
                }
            }

            // If next month started then set end of month and stop.
            if (isNextMonthStarted) {
                sCurrentMonthEndWeekDayIndex = dayOfWeek - 1;
                if (sCurrentMonthEndWeekDayIndex == -1) {
                    sCurrentMonthEndWeekDayIndex = 6;
                    sCurrentMonthEndWeekIndex = week - 1;
                } else {
                    sCurrentMonthEndWeekIndex = week;
                }
                break;// Stop cycle - all variables are set.
            }

            // Add one day.
            calendar.add(Calendar.DAY_OF_YEAR, 1);

            // If other week then increase count of weeks.
            if (++dayOfWeek == 7) {
                week++;
                dayOfWeek = 0;
            }
        }
    }

    public static int getCurrentWeekIndex() {
        return sCurrentWeekIndex;
    }

    public static int getCurrentDayInWeek() {
        return sCurrentDayInWeek;
    }

    public static int getCurrentMonthStartWeekIndex() {
        return sCurrentMonthStartWeekIndex;
    }

    public static int getCurrentMonthStartWeekDayIndex() {
        return sCurrentMonthStartWeekDayIndex;
    }

    public static int getCurrentMonthEndWeekIndex() {
        return sCurrentMonthEndWeekIndex;
    }

    public static int getCurrentMonthEndWeekDayIndex() {
        return sCurrentMonthEndWeekDayIndex;
    }

    public static Locale getCurrentLocale() {
        return sContext.getResources().getConfiguration().locale;
    }

    public static void updateFromPreferences(boolean isSyncWithFolder, String syncFolderPath,
            int notesOrderTypeIndex, boolean isSetDateInNewNote, int alarmsOrderTypeIndex,
            long timeDelay, boolean isDeleteOnHappen, String alarmMusicPath,
            boolean isVibroEnabled, String period, long bother) {
        sSyncWithFolder = isSyncWithFolder;
        sSyncFolderPath = syncFolderPath;
        sNotesOrder = NotesOrder.values()[notesOrderTypeIndex];
        sIsSetDateInNewNote = isSetDateInNewNote;
        sAlarmsOrder = AlarmsOrder.values()[alarmsOrderTypeIndex];
        sTimeDelay = timeDelay;
        sIsDeleteOnHappen = isDeleteOnHappen;
        sAlarmMusicPath = alarmMusicPath;
        sIsVibroEnabled = isVibroEnabled;
        sPeriod = period;
        sBother = bother;
    }

    public static synchronized DBHelper getDbHelper() {
        return sDbHelper;
    }

    public static boolean isSyncWithFolder() {
        return sSyncWithFolder;
    }

    public static String getSyncFolderPath() {
        return sSyncFolderPath;
    }

    public static NotesOrder getNotesOrder() {
        return sNotesOrder;
    }

    public static boolean isSetDateInNewNote() {
        return sIsSetDateInNewNote;
    }

    public static AlarmsOrder getAlarmsOrder() {
        return sAlarmsOrder;
    }

    public static long getTimeDelay() {
        return sTimeDelay;
    }

    public static boolean isDeleteOnHappen() {
        return sIsDeleteOnHappen;
    }

    public static String getAlarmMusicPath() {
        return sAlarmMusicPath;
    }

    public static boolean isVibroEnabled() {
        return sIsVibroEnabled;
    }

    public static String getAlarmPeriod() {
        return sPeriod;
    }

    public static long getBother() {
        return sBother;
    }
}
